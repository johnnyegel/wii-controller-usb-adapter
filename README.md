# WII Controller Adapter board

The WII Controller Adapter board supports connecting two WII Classic controllers and exposes these as
two joysticks on the USB port. It's based on the Black Pill STM32 board.

WARNING! This is currently work in progress, and not yet a complete working project.

## Hardware Connections

The pins of the Black Pill board are connected as follows:

- **Bootloader**:
    - **H1, B13** - Boot Button, Active Low (PB13)
    - **H1, B12** - Internal LED, Active High (PB12) (fixed)

- **USB:**
    - **H1, A11** - USB DM (PA11)
    - **H1, A12** - USB DP (PA12)

- **GamePad 1:**
    - **H2, B8** - I2C1 SCL (PB8)
    - **H2, B9** - I2C1 SDA (PB9)
    - **H2, C13** - GamePad 1 Detect, Active High (PC13)
    - **H2, A0** - GamePad 1 LED GREEN, Active High (PA0) -> TIM2_CH1_ETR
    - **H2, A1** - GamePad 1 LED RED, Active High (PA1)   -> TIM2_CH2

- **GamePad 2:**
    - **H2, B10** - I2C2 SCL (PB10)
    - **H2, B11** - I2C2 SDA (PB11)
    - **H2, B1** - GamePad 2 Detect, Active High (PB1)
    - **H2, A6** - GamePad 2 LED RED, Active High (PA6)   -> TIM3_CH1
    - **H2, A7** - GamePad 2 LED GREEN, Active High (PA7) -> TIM3_CH2

Pinout for the Wii Controller connector can be found here 
(Pin 3 is used to detect a controller present):
https://allpinouts.org/pinouts/connectors/videogame/nintendo-wiimote/

# Bulding and flashing

After cloning the repository, you need to check out the submodules. This is done by

```
$ git submodule update --init --recursive
```

Then you need to build the libopencm3 library, as this is not currently handled automatically
by the Makefile. This is however simple enough:

```
$ cd submodules/libopencm3
$ make
$ cd ../..
```

Now we are ready for building. Simply run:

```
$ make
```

The makefile also supports flashing via an STLinkV2 connected to the Black Pill board. Simply run:

```
$ make flash
```

Flashing via STLinkV2 installs a bootloader (dapboot), which can be invoked by pushing the button while connecting the 
USB cable. It can also be accessed via the CDC virtual console via the command 'dfu'.

The make file can then flash via the DFU bootloader. Once the device is in bootloader mode, run the following command:

```
$ make dfu
```

NOTE! You will not be able to update the bootloader itself via DFU (natrually), but normally you won't change the bootloader anyway.
