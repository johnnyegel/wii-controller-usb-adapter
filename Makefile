##============================================================================================
## Make script for building the wii controller adapter project and it' dependencies.
##
## The following targets may be interesting:
##   make - Build the target ELF and HEX files
##   make libopencm3 - Builds the LibOpenCM3 libraries
##   make bootdfu - Build the DFU bootloader
##
##============================================================================================

# The project name, which in turn is used to generate the output
PROJECT = wiiadapter-v1.0

# Define object files in relation to their location in SRCPATH
#-------------------------------------------------------------
SOURCES =  main.c
SOURCES += system/systime.c
SOURCES += system/system_core.c
SOURCES += driver/leds.c
SOURCES += driver/i2cext.c
SOURCES += driver/wiictrl.c
SOURCES += driver/bootloader.c
SOURCES += util/scheduler.c
SOURCES += usb/usbext.c
SOURCES += usb/wcausb.c
SOURCES += usb/cdc-serial.c
SOURCES += usb/hid-gamepad.c
SOURCES += tasks/testtask.c
SOURCES += tasks/console.c
SOURCES += tasks/wiictrltask.c
#-------------------------------------------------------------

# Define the linker script
LDSCRIPT = link/black-pill-dfuboot.ld

# Paths configuration
OPENCM3_DIR = submodules/libopencm3
SRCPATH = src
INCPATHS = ${SRCPATH} ${OPENCM3_DIR}/include
BUILDROOT = build
BUILD_MAIN = ${BUILDROOT}/wiiadapter

# Constants for the project
MCUTYPE		= STM32F1
LIBNAME		= opencm3_stm32f1
BOOTLOADER  = dapboot

#---------------------------------------------------------------------------------------------
# Flags for the target architecture
LDFLAGS		+= -L$(OPENCM3_DIR)/lib
LDLIBS		+= -l$(LIBNAME)

DEFS		:= $(addprefix -I,${INCPATHS})
DEFS		+= -D${MCUTYPE} 

FP_FLAGS	?= -msoft-float
ARCH_FLAGS	= -mthumb -mcpu=cortex-m3 $(FP_FLAGS) -mfix-cortex-m3-ldrd 

#=============================================================================================

# Be silent per default, but 'make V=1' will show all compiler calls.
ifneq ($(V),1)
Q		:= @
NULL	:= 2>/dev/null
endif

###############################################################################
# Executables

PREFIX	?= arm-none-eabi-

CC		:= $(PREFIX)gcc
CXX		:= $(PREFIX)g++
LD		:= $(PREFIX)gcc
OBJCOPY	:= $(PREFIX)objcopy
OBJDUMP	:= $(PREFIX)objdump
GDB		:= $(PREFIX)gdb
SIZE	:= $(PREFIX)size
STFLASH	= $(shell which st-flash)
DFUUTIL	= $(shell which dfu-util)
SRECCAT = $(shell which srec_cat)
SRECINF = $(shell which srec_info)
OPT		:= -Os
DEBUG	:= -ggdb3
CSTD	?= -std=c99

###############################################################################
# C flags

TGT_CFLAGS	+= $(OPT) $(CSTD) $(DEBUG)
TGT_CFLAGS	+= $(ARCH_FLAGS)
TGT_CFLAGS	+= -Wextra -Wshadow -Wimplicit-function-declaration
TGT_CFLAGS	+= -Wredundant-decls -Wmissing-prototypes -Wstrict-prototypes
TGT_CFLAGS	+= -fno-common -ffunction-sections -fdata-sections

###############################################################################
# C++ flags

TGT_CXXFLAGS	+= $(OPT) $(CXXSTD) $(DEBUG)
TGT_CXXFLAGS	+= $(ARCH_FLAGS)
TGT_CXXFLAGS	+= -Wextra -Wshadow -Wredundant-decls  -Weffc++
TGT_CXXFLAGS	+= -fno-common -ffunction-sections -fdata-sections

###############################################################################
# C & C++ preprocessor common flags

TGT_CPPFLAGS	+= -MD
TGT_CPPFLAGS	+= -Wall -Wundef
TGT_CPPFLAGS	+= $(DEFS)

###############################################################################
# Linker flags

TGT_LDFLAGS		+= --static -nostartfiles --specs=nano.specs
TGT_LDFLAGS		+= -T$(LDSCRIPT)
TGT_LDFLAGS		+= $(ARCH_FLAGS) $(DEBUG)
TGT_LDFLAGS		+= -Wl,--cref -Wl,--print-memory-usage
TGT_LDFLAGS		+= -Wl,--gc-sections
ifeq ($(V),99)
TGT_LDFLAGS		+= -Wl,--print-gc-sections
endif

###############################################################################
# Used libraries

LDLIBS		+= -Wl,--start-group -lc -lgcc -specs=nosys.specs -Wl,--end-group

# Generate object file list
objstmp := ${SOURCES}
objstmp := $(objstmp:.c=.o)
objstmp := $(objstmp:.cpp=.o)
objstmp := $(objstmp:.cxx=.o)
deps=$(addprefix ${BUILD_MAIN}/,$(objstmp:.o=.d))
OBJS=$(addprefix ${BUILD_MAIN}/,$(objstmp))

#$(info Deps: ${DEPS})

#---------------------------------------------------------------------------------------------
# Compile and linker targets

all: build hex

build: build-boot build-app

build-boot: ${BUILDROOT}/${BOOTLOADER}.elf
	$(SIZE) -x ${BUILDROOT}/${BOOTLOADER}.elf

build-app: ${BUILDROOT}/${PROJECT}.elf
	$(Q)$(SIZE) -x ${BUILDROOT}/${PROJECT}.elf

flash: all
	$(Q)$(STFLASH) --format ihex write ${BUILDROOT}/${PROJECT}-${BOOTLOADER}.hex

dfu: bin-app
	-$(Q)\
	echo "dfu\r\n" > /dev/ttyACM0; sleep 1 && \
	$(DFUUTIL) -D ${BUILDROOT}/${PROJECT}.bin \

bin-app: build-app
	$(Q)$(OBJCOPY) -O binary ${BUILDROOT}/${PROJECT}.elf ${BUILDROOT}/${PROJECT}.bin

hex: hex-boot hex-app
	@printf "  MERGING -> HEX: ${PROJECT}-${BOOTLOADER}.hex\n"
	$(Q)$(SRECCAT) ${BUILDROOT}/${BOOTLOADER}.hex -intel ${BUILDROOT}/${PROJECT}.hex -intel -o ${BUILDROOT}/${PROJECT}-${BOOTLOADER}.hex -intel
	$(Q)$(SRECINF) ${BUILDROOT}/${PROJECT}-${BOOTLOADER}.hex -intel

hex-boot: build-boot
	@printf "  OBJCOPY -> HEX: ${BOOTLOADER}.hex\n"
	$(Q)$(OBJCOPY) -O ihex ${BUILDROOT}/${BOOTLOADER}.elf ${BUILDROOT}/${BOOTLOADER}.hex 

hex-app: build-app
	@printf "  OBJCOPY -> HEX: ${PROJECT}.hex\n"
	$(Q)$(OBJCOPY) -O ihex ${BUILDROOT}/${PROJECT}.elf ${BUILDROOT}/${PROJECT}.hex 

clean: clean-boot clean-app

clean-boot:
	$(Q)$(MAKE) -f ./dapboot.mk clean

clean-app:
	@printf "  CLEAN\n"
	$(Q)$(RM) -rf $(BUILDROOT)

# Phonys
.PHONY: all build build-boot build-app flash dfu-util hex hex-boot hex-app bin-app clean clean-boot clean-app

#---------------------------------------------------------------------------------------------

# Actual file and directory targets
${BUILDROOT}/:
	@mkdir -p ${BUILDROOT}

${BUILDROOT}/${BOOTLOADER}.elf:
	@printf "Building bootloader\n"
	$(Q)$(MAKE) -f ./dapboot.mk all

${BUILDROOT}/${PROJECT}.elf ${BUILDROOT}/${PROJECT}.map: $(OBJS) $(LDSCRIPT) $(OPENCM3_DIR)/lib/lib$(LIBNAME).a | ${BUILDROOT}/
	@printf "  LD      $(PROJECT).elf\n"
	@mkdir -p $(@D)
	$(Q)$(LD) $(TGT_LDFLAGS) $(LDFLAGS) $(OBJS) $(LDLIBS) -Wl,-Map=${BUILDROOT}/$(PROJECT).map -o ${BUILDROOT}/$(PROJECT).elf

${BUILD_MAIN}/%.o: ${SRCPATH}/%.c
	@printf "  CC      $(*).c\n"
	@mkdir -p $(@D)
	$(Q)$(CC) $(TGT_CFLAGS) $(CFLAGS) $(TGT_CPPFLAGS) $(CPPFLAGS) -o ${BUILD_MAIN}/$(*).o -c ${SRCPATH}/$(*).c

${BUILD_MAIN}/%.o: ${SRCPATH}/%.cxx
	@printf "  CXX     $(*).cxx\n"
	@mkdir -p $(@D)
	$(Q)$(CXX) $(TGT_CXXFLAGS) $(CXXFLAGS) $(TGT_CPPFLAGS) $(CPPFLAGS) -o ${BUILD_MAIN}/$(*).o -c ${SRCPATH}/$(*).cxx

${BUILD_MAIN}/%.o: ${SRCPATH}/%.cpp
	@printf "  CXX     $(*).cpp\n"
	@mkdir -p $(@D)
	$(Q)$(CXX) $(TGT_CXXFLAGS) $(CXXFLAGS) $(TGT_CPPFLAGS) $(CPPFLAGS) -o ${BUILD_MAIN}/$(*).o -c ${SRCPATH}/$(*).cpp

-include ${deps}
