/*
 * systime.c
 *
 *  Created on: Dec 22, 2018
 *      Author: johnny
 */

#include <system/systime.h>
#include <libopencmsis/core_cm3.h>
#include <libopencm3/cm3/nvic.h>
#include <libopencm3/cm3/systick.h>
#include <libopencm3/stm32/rcc.h>

// The reload time for the systick counter
//#define SYSTIME_RELOAD_COUNT	(MCU_CLOCK_SPEED / 8 / 1000)

// The system time
volatile uint32_t systime_now = 0;

//====================================================================
// Systick IRQ handler
//====================================================================

/**
 * Place the IRQ handler in CCM, to make it as fast as possible
 */
void sys_tick_handler(void) {
	systime_now ++;
}

//====================================================================
// API Implementation
//====================================================================


void systime_init(uint8_t priority) {
	// Initialize the system time
	systime_now = 0;

   	// Set Systick priority (give it priority 8 (0 highest, 16 lowest))
	// NOTE! This function actually handles priority of systick IRQ, even if this is
	//       not an NVIC IRQ.
	nvic_set_priority(NVIC_SYSTICK_IRQ, priority);

	// Set the systick clock source and counter wrap
	systick_set_reload(rcc_ahb_frequency / 1000);
	systick_set_clocksource(STK_CSR_CLKSOURCE_AHB);

	// Clear the timer
	systick_clear();

	// Enable the counter
	systime_set_enabled(true);
}

void systime_set_enabled(bool enable) {
	if (enable) {
		// Enable the interrupt
		systick_interrupt_enable();

		// Start the systick counter
		systick_counter_enable();
	}
	else {
		// Diable counter
		systick_counter_disable();

		// Disable the systick interrupt
		systick_interrupt_disable();

		// Clear the timer
		systick_clear();
	}
}

void systime_delay(uint32_t millisec) {
	uint32_t starttime = systime_now;
	while((systime_now - starttime) < millisec) {
		__WFI();
	}
}
