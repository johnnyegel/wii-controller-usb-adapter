/*
 * syscalls.c
 *
 *  Created on: Dec 22, 2018
 *      Author: johnny
 */

#include <errno.h>
#include <stdio.h>
#include <unistd.h>

#include <libopencm3/cm3/nvic.h>
#include <system/system_core.h>
#include <system/systime.h>
#include <usb/cdc-serial.h>
//#include <errno.h>
//#include <sys/unistd.h> // STDOUT_FILENO, STDERR_FILENO


// If set to 1, the _write function will spinlock the CPU on
// contention for the given amount of milliseconds.
#define SYSCALLS_CONTENTION_WAIT	10

//==========================================================================
// System core functions
//==========================================================================

// Must define this to avoid compiler warning
int _write (int fd, const void *buf, size_t count);

/**
 * Implements redirection of printf's to the USB port
 */
int _write (int fd, const void *buf, size_t count) {
   if ((fd != STDOUT_FILENO) && (fd != STDERR_FILENO)) {
      errno = EBADF;
      return -1;
   }

   // TODO: Spin locking here causes CPU contentions, slowing everything down.
   //       There should be a mechanism which controls this behavior.
#ifdef SYSCALLS_CONTENTION_WAIT
   if (cdcacm_send_avail() < count) {
	   uint32_t wstart = systime_now;
	   // Spin lock until there is room for the data we want to send.
	   while (cdcacm_send_avail() < count) {
		   // Or we time out.
		   if ((systime_now - wstart) > SYSCALLS_CONTENTION_WAIT) return 0;
	   }
   }
#endif

   // Dump the data to the USB serial
   return cdcacm_send_data((uint8_t*)buf, count);
}

/**
 * Hard fault handler, called when shit hits the fan
 */
void hard_fault_handler(void) {
   // Loop indefinitely
   int debug_changeable_flag = 0;
   while (debug_changeable_flag == 0);
}

//==========================================================================
// API Functions
//==========================================================================

/**
 * Syscall initialization
 */
void system_core_init(void) {
	// Do a bogus write call, in order to have the compiler and linker behave!
	_write(99, NULL, 0);
}

