/*
 * systime.h
 *
 * Functions used to control system timing. It runs of the systick timer.
 *
 *  Created on: Dec 22, 2018
 *      Author: johnny
 */

#ifndef SYSTEM_SYSTIME_H_
#define SYSTEM_SYSTIME_H_

#include <stdint.h>
#include <stdbool.h>

/**
 * The current system time value in milliseconds since systime_init
 */
extern volatile uint32_t systime_now;

// Compatibility macro, as many libraries use system_millis
#define system_millis	(systime_now)

/**
 * Initialize the system time handler
 * 
 * The priority defines the IRQ priority of the systick interrupt. By default
 * this is 0 (highest priority), but unless the accuracy of the systick is very
 * important to the application, it's suggested to lower this. 0x80 is a good value.
 * 
 * @param priority The priority of the systick IRQ.
 */
void systime_init(uint8_t priority);

/**
 * Delay for the given number of milliseconds
 * 
 * WARNING! If the systime is disables, this will lock up indefinitely!
 */
void systime_delay(uint32_t millisec);

/**
 * Enables or disables the systime timer
 * 
 * Disabling the system timer will result in no ticks being counted.
 * 
 * @param enable True to enable the systime, and false to disable it
 */
void systime_set_enabled(bool enable);

/**
 * Typedef for timer handle
 */
typedef uint32_t systime_timer_t;

/**
 * Resets a timer value and puts it into a timer handle
 */
static inline void systime_timer_reset(systime_timer_t* timer) {
    *timer = systime_now;
}

/**
 * Checks if "timeout" ticks has elapsed since given timer was reset;
 * 
 * @param timer Timer to check if timeout has elapsed
 * @param timeout Number of ticks since reset, before the timer has elapsed
 * @return True if elapsed, and false if notk
 */
static inline bool systime_timer_elapsed(systime_timer_t* timer, int32_t timeout) {
    return (int32_t)(systime_now - (*timer + timeout)) >= 0; 
}

#endif /* SYSTEM_SYSTIME_H_ */
