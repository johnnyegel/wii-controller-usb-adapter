/*
 * syscalls.h
 *
 *  Created on: Dec 22, 2018
 *      Author: johnny
 */

#ifndef SYSTEM_SYSTEM_CORE_H_
#define SYSTEM_SYSTEM_CORE_H_

#include <stdint.h>

/**
 * Initializes the syscalls hooks
 */
void system_core_init(void);



#endif /* SYSTEM_SYSCALLS_H_ */
