/**
 * wiictrl.h - Driver for Wii controller interface
 */
#ifndef _DRIVER_WIICTRL_H
#define _DRIVER_WIICTRL_H

#include <stdint.h>

#define WIICTRL_COUNT   2

/**
 * Defines the state the WII controller port is currently in
 */
enum wiictrl_status {
    WIICTRL_STATUS_DISCONNECTED = 0,
    WIICTRL_STATUS_UNIDENTIFIED,
    WIICTRL_STATUS_CONNECTED,
    WIICTRL_STATUS_FAILED
};

/**
 * Generic structure holing the gamepad layout
 */
struct wiictrl_gamepad {
    uint16_t   buttons;
    int8_t     lx, ly;
    int8_t     rx, ry;
    int8_t     lt, rt;
}
__attribute__((packed));


/**
 * Initialize Wii controller communication
 */
void wiictrl_init(void);

/**
 * Updates controller information for the specified controller number
 * 
 * This should be called at a rate defined by the wanted refresh rate.
 * 
 * @return True if controller was detected and successfully read, and false if not.
 */
enum wiictrl_status wiictrl_update_info(struct wiictrl_gamepad* gp, uint32_t ctrlnum);

#endif