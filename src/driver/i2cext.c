/**
 * i2cext.h - Implements higher level I2C communication functions that don't suck (as the stock libopencm3 stuff)
 */

#include <driver/i2cext.h>
#include <system/systime.h>

// Define error bits to check for
#define I2CEXT_SR1_ERRORS      (I2C_SR1_PECERR | I2C_SR1_BERR | \
                               I2C_SR1_AF | I2C_SR1_ARLO | I2C_SR1_OVR)

// Timeout happens after 10 ticks (ms), which is plenty of time
#define I2CEXT_TIMEOUT      10

/**
 * Spin-locks until one of the following conditions happens:
 * - An I2C error flag is raised - A stop is sent, the error is cleared and false is returned
 * - A specified SR1 flag is matched - True is returned
 * 
 * - A specified SR2 flag or BUSY  is matched - 
 */
static inline bool _i2cext_wait(uint32_t i2c, uint16_t sr1_flags) {
    uint16_t flags;
    systime_timer_t timeout;

    // Set up the timer
    systime_timer_reset(&timeout);

    // Loop until timeout
    while(!systime_timer_elapsed(&timeout, I2CEXT_TIMEOUT)) {
        // Grab the SR1 bits
        flags = I2C_SR1(i2c);

        // Check for errors
        if (flags & I2CEXT_SR1_ERRORS) break;

        // Check for SR1 flags
        if (sr1_flags) {
            // No Errors, check for SR1 flag match
            if (flags & sr1_flags) return true;
        }
        else {
            // Grab SR2
            flags = I2C_SR2(i2c);

            // Finally, check if busy is cleared, which means there is nothing more to wait for
            if (!(flags & I2C_SR2_BUSY)) return true;
        }
    }

    // Since we are a master, send stop condition
    i2c_send_stop(i2c);
    // Clear all errors.
    I2C_SR1(i2c) = 0;
    // Return false to indicate an error
    return false;
}

void i2cext_init(uint32_t i2c) {

    // TODO: The resulting data rate is a little bit low!

    // -- Set up I2C --
    /* Disable the I2C before changing any configuration. */
    i2c_peripheral_disable(i2c);
    i2c_reset(i2c);
    
    /* APB1 is running at 36MHz. */
    i2c_set_clock_frequency(i2c, I2C_CR2_FREQ_36MHZ);

    /* 400KHz - I2C Fast Mode */
    i2c_set_fast_mode(i2c);

    /*
    * fclock for I2C is 36MHz APB2 -> cycle time 28ns, low time at 400kHz
    * incl trise -> Thigh = 1600ns; CCR = tlow/tcycle = 0x1C,9;
    * Datasheet suggests 0x1e.
    */
    i2c_set_ccr(i2c, 0x1e);

    /*
    * fclock for I2C is 36MHz -> cycle time 28ns, rise time for
    * 400kHz => 300ns and 100kHz => 1000ns; 300ns/28ns = 10;
    * Incremented by 1 -> 11.
    */
    i2c_set_trise(i2c, 0x0b);

    /* If everything is configured -> enable the peripheral. */
    i2c_peripheral_enable(i2c);
}




bool i2cext_write7(uint32_t i2c, uint8_t addr, const uint8_t* data, size_t len) {
    // Wait if the I2C interface is busy. Ignore errors.
    _i2cext_wait(i2c, 0);

    // Send start but
    i2c_send_start(i2c);

    // Wait for Start condition generated or master mode flag
    if (!_i2cext_wait(i2c, I2C_SR1_SB)) return false;

    // Send 7-bit address
    i2c_send_7bit_address(i2c, addr, I2C_WRITE);

    // Wait for address transferred, and clear the address condition
    if (!_i2cext_wait(i2c, I2C_SR1_ADDR)) return false;
    (void)I2C_SR2(i2c);

    // Send the data
    for (size_t i = 0; i < len; i++) {
        // Wait until ready to send byte
        if (!_i2cext_wait(i2c, I2C_SR1_TxE)) return false; 

        // Send next byte
        i2c_send_data(i2c, data[i]);
    }

    // Send stop condition
    i2c_send_stop(i2c);
    
    return true;
}

bool i2cext_read7(uint32_t i2c, uint8_t addr, uint8_t *data, size_t len) {
    // Wait until bus is not busy
    _i2cext_wait(i2c, 0);

    // Send start bit and enable acknowledge
    i2c_send_start(i2c);
    i2c_enable_ack(i2c);

    // Wait for master mode to be selected
    if (!_i2cext_wait(i2c, I2C_SR1_SB)) return false;

    // Send address and Read bit
    i2c_send_7bit_address(i2c, addr, I2C_READ);

    // Wait for address to be transfered
    if (!_i2cext_wait(i2c, I2C_SR1_ADDR)) return false;
    (void)I2C_SR2(i2c);

    for (size_t i = 0; i < len; i++) {
        // There is no need to wait if this is the first read byte.
        if (!_i2cext_wait(i2c, I2C_SR1_RxNE | I2C_SR1_BTF)) return false;
 
        // After the last byte is read, disable ack, and set up stop condition
        if (i == len - 1) {
            i2c_disable_ack(i2c);
            i2c_send_stop(i2c);
        }
        // Get the data, and increase i
        data[i] = i2c_get_data(i2c);
    }

    return true;
}