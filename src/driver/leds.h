/**
 * driver/leds.h
 * 
 * Driver for the LEDs for the controller
 */
#ifndef _DRIVER_LEDS_H
#define _DRIVER_LEDS_H

#include <stdint.h>
#include <stdbool.h>


/* Define the LEDs */
enum led_id {
    LED_INTERNAL = 0,
    LED_GP1_RED,
    LED_GP1_GRN,
    LED_GP2_RED,
    LED_GP2_GRN,
    LED__COUNT
};

/**
 * Initialize the LED driver
 */
void leds_init(void);

/**
 * Set LED state
 * 
 * If a brightness level is set, this will be take into account
 * 
 * @param led The led to Set state to
 * @param state True to turn on and false to turn off
 */
void led_set(enum led_id led, bool state);

/**
 * Sets LED brightness value. 
 * 
 * By default this is UINT16_MAX.
 */
void led_brightness(enum led_id led, uint16_t brightness);

/**
 * Toggles LED state
 */
//void led_toggle(enum led_id led);

/**
 * Turns a LED on
 * 
 * @param led The LED to turn ON
 */
static inline void led_on(enum led_id led) {
    led_set(led, true);
}

/**
 * Turns a LED off
 * 
 * @param led The LED to turn OFF
 */
static inline void led_off(enum led_id led) {
    led_set(led, false);
}

#endif