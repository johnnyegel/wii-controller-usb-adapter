/*
 * gpioext.h
 *
 * Extension to the GPIO api
 *
 *  Created on: Mar 3, 2019
 *      Author: johnny
 */

#ifndef UTIL_GPIOEXT_H_
#define UTIL_GPIOEXT_H_

#include <libopencm3/stm32/gpio.h>
#include <stdint.h>

/**
 * Structure which can be used to define a GPIO port and PIN
 */
typedef struct {
	uint32_t port;
	uint32_t pin;
}
gpioext_gpiodef_t;


/**
 * Writes data to GPIO port, but only affects bits matching the mask.
 *
 * If a bit matches the mask, it will be set or cleared according to the corresponding
 * bits in 'data'. This makes it very simple to change multiple GPIO pins at once, without
 * affecting unrelated pins.
 *
 * @param gpioport The GPIO port to write to
 * @param mask The bitmask indicating which pins should be affected by the write
 * @param data Corresponding bits which defines the GPIO states to write to the pins defined in the mask
 */
static inline void gpioext_masked_write(uint32_t gpioport, uint16_t mask, uint16_t data) {
	//GPIO_BSRR(gpioport) = (data & mask) | ((~data & mask) << 16);

	// NOTE! The BSRR doc says a Set will win over a clear. This means we can simply use the mask
	//       as the clear part, as any bits we would like to be set will be covered by the data
	GPIO_BSRR(gpioport) = (data & mask) | (mask << 16);
}

#endif /* UTIL_GPIOEXT_H_ */
