/**
 * bootloader.c
 * 
 * API to access bootloader
 */

#include <libopencm3/stm32/rcc.h>
#include <libopencm3/stm32/rtc.h>
#include <libopencm3/stm32/pwr.h>
#include <libopencm3/cm3/scb.h>

#include <driver/bootloader.h>


#define RTC_BKP_DR(reg)  MMIO16(BACKUP_REGS_BASE + 4 + (4 * (reg)))

static const uint32_t CMD_BOOT = 0x544F4F42UL;

enum BackupRegister {
    BKP0 = 0,
    BKP1,
    BKP2,
    BKP3,
    BKP4,
};

static void _bootloader_backup_write(enum BackupRegister reg, uint32_t value) {
    rcc_periph_clock_enable(RCC_PWR);
    rcc_periph_clock_enable(RCC_BKP);

    pwr_disable_backup_domain_write_protect();
    RTC_BKP_DR((int)reg*2) = value & 0xFFFFUL;
    RTC_BKP_DR((int)reg*2+1) = (value & 0xFFFF0000UL) >> 16;
    pwr_enable_backup_domain_write_protect();
}

void bootloader_reset_and_enter(void) {
    // Write magic string to backup registers
    _bootloader_backup_write(BKP0, CMD_BOOT);

    // Reset the chip
    scb_reset_system();

    // Halt
    while(1);
}