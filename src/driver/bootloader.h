/**
 * bootloader.h
 * 
 * API to access bootloader
 */

#ifndef _DRIVER_BOOTLOADER_H
#define _DRIVER_BOOTLOADER_H

/**
 * Resets the chip and starts the bootloader
 */
void bootloader_reset_and_enter(void);

#endif