/**
 * i2cext.h - Implements higher level I2C communication functions that don't suck (as the stock libopencm3 stuff)
 */

#ifndef _DRIVER_I2CEXT_H
#define _DRIVER_I2CEXT_H

#include <stdint.h>
#include <stddef.h>
#include <stdbool.h>

#include <libopencm3/stm32/i2c.h>

/**
 * Initializes the I2C interface
 */
void i2cext_init(uint32_t i2c);

/**
 * Write data to an I2C device using 7-bit addressing
 * 
 * Repeat start can be used to send mulitple sequences back to back. If "stop" is false, no
 * stop bit is sent, and a repeated start is sent the next time a write or read is made.
 * 
 * @param i2c The I2C interface to use
 * @param addr The device address to write to
 * @param data The data buffer to write from
 * @param len The length of the data to write
 * @return true if the write sequence was successful, and false if it failed
 */
bool i2cext_write7(uint32_t i2c, uint8_t addr, const uint8_t* data, size_t len);
  
/**
 * Read data from an I2C device into the specified buffer
 * 
 * Repeat start can be used to send mulitple sequences back to back. If "stop" is false, no
 * stop bit is sent, and a repeated start is sent the next time a write or read is made.
 *
 * @param i2c The I2C interface to use
 * @param addr The device address to read from
 * @param data The data buffer to read into
 * @param len The number of bytes (frames) to read
 * @return true if the read sequence was successful, and false if it failed
 */
bool i2cext_read7(uint32_t i2c, uint8_t addr, uint8_t *data, size_t len);

#endif