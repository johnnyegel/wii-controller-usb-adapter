/**
 * driver/leds.h
 * 
 * LED Driver implementation
 */

#include <libopencm3/stm32/rcc.h>
#include <libopencm3/stm32/timer.h>
#include <driver/gpioext.h>

#include <driver/leds.h>
#include <stddef.h>

// Table defining all the LEDs
static const gpioext_gpiodef_t LEDS[] = {
    {GPIOB, GPIO12},
    {GPIOA, GPIO1}, 
    {GPIOA, GPIO0}, 
    {GPIOA, GPIO6}, 
    {GPIOA, GPIO7},
};
// Define which Pin state turns the LED on
static const uint8_t LACH[] = {0, 1, 1, 1, 1};

// Set up list of timers and channels related to the PWM generation
static const          uint32_t LTIMER[] = {0, TIM2, TIM2, TIM3, TIM3};
static const    enum tim_oc_id LTIMCH[] = {0, TIM_OC2, TIM_OC1, TIM_OC1, TIM_OC2};

#define LEDSSIZE         (sizeof(LEDS) / sizeof(gpioext_gpiodef_t))
#define LEDINVALID(l)    ((uint32_t)l >= LEDSSIZE)

/*
// Keep the state of each LED
static struct {
    uint16_t    brightness;
    bool        state;
}
_ledstate[LEDSSIZE];
*/

static void _leds_set_pwm_mode(enum led_id led, bool enable) {
    if (led <= 0) return;
    if (enable) {
        gpio_set_mode(LEDS[led].port, GPIO_MODE_OUTPUT_50_MHZ, GPIO_CNF_OUTPUT_ALTFN_PUSHPULL, LEDS[led].pin);
    }
    else {
        gpio_set_mode(LEDS[led].port, GPIO_MODE_OUTPUT_50_MHZ, GPIO_CNF_OUTPUT_PUSHPULL, LEDS[led].pin);
    }
}

static void _leds_init_pwm_timer(uint32_t timer) {
    // Apply the exact same configuration to timer 4.
    timer_set_mode(timer, TIM_CR1_CKD_CK_INT, TIM_CR1_CMS_EDGE, TIM_CR1_DIR_UP);
    timer_set_period(timer, UINT16_MAX);
    timer_set_prescaler(timer, 0);

    // Enable output compare mode and PWM2 on both relevant channels
    timer_set_oc_mode(timer, TIM_OC1, TIM_OCM_PWM1);
    timer_set_oc_mode(timer, TIM_OC2, TIM_OCM_PWM1);
    timer_enable_oc_output(timer, TIM_OC1);
    timer_enable_oc_output(timer, TIM_OC2);

    // Turn on both timers.
    timer_enable_counter(timer);
}


void leds_init(void) {
    // Set up the clocks for the GPIOs
    rcc_periph_clock_enable(RCC_GPIOA);
    rcc_periph_clock_enable(RCC_GPIOB);

    // Enable the timer clocks
    rcc_periph_clock_enable(RCC_TIM2);
    rcc_periph_clock_enable(RCC_TIM3);

    // Initialize the two timers
    _leds_init_pwm_timer(TIM2);
    _leds_init_pwm_timer(TIM3);

    // Configure all the LEDs as output
    for (uint32_t i = 0; i < LEDSSIZE; i++) {
        // Set up the pin, and turn it off
        _leds_set_pwm_mode(i, false);

        // Initial state is off, and brightness is full
        led_set(i, false);
        led_brightness(i, UINT16_MAX);
    }

}

void led_brightness(enum led_id led, uint16_t brightness) {
    if (led <= 0 || LEDINVALID(led)) return;

    // Determine if the brightness value needs to change
    timer_set_oc_value(LTIMER[led], LTIMCH[led], brightness);
}

void led_set(enum led_id led, bool state) {
    if (LEDINVALID(led)) return;

    // PWM mode follows the state regardless if the output is inverted
    _leds_set_pwm_mode(led, state);

    // Invert state for LEDs which are active low
    if (state ^ !LACH[led]) {
        gpio_set(LEDS[led].port, LEDS[led].pin);
    }
    else {
        gpio_clear(LEDS[led].port, LEDS[led].pin);
    }
}
