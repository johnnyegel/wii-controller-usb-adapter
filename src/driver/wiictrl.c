/**
 * wiictrl.c - Driver for Wii controller interface
 */

#include <string.h>

#include <driver/wiictrl.h>
#include <driver/gpioext.h>
#include <driver/i2cext.h>
#include <system/systime.h>

#include <libopencm3/stm32/rcc.h>

// I2C settings
#define WIICTRL_I2C_ADDR            0x52
#define WIICTRL_I2C_READLEN         6

// Detect debounce setting
#define WIICTRL_DETECT_DEBOUNCE     5

// Number of failed Gamepad readings until the gamepad is re-initialized
#define WIICTRL_MAX_FAIL_COUNT      3

/**
 * Define state transition counters
 */ 
// const uint16_t _WIICTRL_COUNTS[] = {
//     0,   /* WIICTRL_STATUS_DISCONNECTED */
//     0,   /* WIICTRL_STATUS_UNIDENTIFIED */
//     0,   /* WIICTRL_STATUS_CONNECTED */
//     0,   /* WIICTRL_STATUS_FAILED */
// };

// Define information for the related peripherals
const struct wiictrl_def {
    uint32_t i2cid;
    gpioext_gpiodef_t detect;    
} 
_WIICTRL_DEF[WIICTRL_COUNT] = {
    {I2C1, {GPIOC, GPIO13 }},
    {I2C2, {GPIOB, GPIO1 }},
};

// Enumeration defining the available types of controllers
enum wiictrl_type {
    WIICTRL_TYPE_UNSUPPORTED = -1,
    WIICTRL_TYPE_UNKNOWN = 0,
    WIICTRL_TYPE_WII_CLASSIC,
    WIICTRL_TYPE_WII_CLASSIC_PRO,
};


// Keep an array of signatures which relates to the controller types
/**
 * 0000 A420 0000	Nunchuk
 * 0000 A420 0101	Classic Controller
 * 0100 A420 0101	Classic Controller Pro
 * FF00 A420 0013	Drawsome Graphics Tablet
 * 0000 A420 0103	GH3 or GHWT Guitar
 * 0100 A420 0103	Guitar Hero World Tour Drums
 */
const uint8_t WIICTRL_SIGNATURES[][6] = {
    {0x00, 0x00, 0xA4, 0x20, 0x01, 0x01},
    {0x01, 0x00, 0xA4, 0x20, 0x01, 0x01},
};

// Keeps the states
static struct wiictrl_curr_state {
    enum wiictrl_type    type;
    enum wiictrl_status  status;
    //uint16_t             iter_cnt;
    uint16_t             fail_count;
    uint16_t             detect_cnt;
    bool                 detect_state;
}
_wiictrl_state[WIICTRL_COUNT];

#define R(i, m)       (_readbuffer[i] & m)
#define AC(b)         ((b) - 0x80)
#define EX(t, r)      (((t) & (0xFF << (8 - (r)))) | ((t) >> (r)))

// Buffer to keep read data in
static uint8_t _readbuffer[WIICTRL_I2C_READLEN];


static inline void _wiictrl_init_gpio(uint32_t i) {
    // Grab the GPIO pin
    const gpioext_gpiodef_t* gpio = &_WIICTRL_DEF[i].detect;

    // Set Input with Pull Up/Down enabled. Enable Pull Down by setting ODR to 0.
    gpio_set_mode(gpio->port, GPIO_MODE_INPUT, GPIO_CNF_INPUT_PULL_UPDOWN, gpio->pin);
    gpio_clear(gpio->port, gpio->pin);
}

static inline void _wiictrl_disable_reset_i2c(uint32_t i) {
    uint32_t i2cid = _WIICTRL_DEF[i].i2cid; 
    i2c_peripheral_disable(i2cid);
    i2c_reset(i2cid);
}


static inline void _wiictrl_init_enable_i2c(uint32_t i) {
    // Then initialize the I2C
    uint32_t i2cid = _WIICTRL_DEF[i].i2cid; 
    i2cext_init(i2cid);
}

/**
 * Identifies which controller is connected
 */
static bool _wiictrl_initialize_controller(uint32_t ctrlnum) {
    // Get the I2C ID
    uint32_t i2cid = _WIICTRL_DEF[ctrlnum].i2cid; 

    // Get pointer to state
    struct wiictrl_curr_state* cst = &_wiictrl_state[ctrlnum];

    // Initially we set the type as unknown
    cst->type = WIICTRL_TYPE_UNKNOWN;

    // Initialize encryptioon so it's effectively disabled
    const uint8_t init1[] = { 0xF0, 0x55 };
    if (!i2cext_write7(i2cid, WIICTRL_I2C_ADDR, init1, sizeof(init1))) return false;

    const uint8_t init2[] = { 0xFB, 0x00 };
    if (!i2cext_write7(i2cid, WIICTRL_I2C_ADDR, init2, sizeof(init2))) return false;

    // Request 6 identification bytes
    const uint8_t idreq[] = { 0xFA };
    if (!i2cext_write7(i2cid, WIICTRL_I2C_ADDR, idreq, sizeof(idreq))) return false;
    if (!i2cext_read7(i2cid, WIICTRL_I2C_ADDR, _readbuffer, 6)) return false;

    // Detect the type of controller
    cst->type = WIICTRL_TYPE_UNSUPPORTED;
    for (uint32_t i = 0; i < sizeof(WIICTRL_SIGNATURES) / sizeof(WIICTRL_SIGNATURES[0]); i++) {
        if (memcmp(WIICTRL_SIGNATURES[i], _readbuffer, sizeof(WIICTRL_SIGNATURES[0])) == 0) {
            cst->type = (enum wiictrl_type) (i + 1);
            break;
        }
    }

    // If unsupported, return false
    return cst->type != WIICTRL_TYPE_UNSUPPORTED;
}

static inline void _wiictrl_decode_wiiclassic(struct wiictrl_gamepad* const gp) {
    // Temporary holder of data
    uint8_t temp;

    // Decode the stuff and put it into the struct
    temp = AC(R(0, 0x3F) << 2); 
    gp->lx = EX(temp, 6);

    temp = AC(R(1, 0x3F) << 2); 
    gp->ly = EX(temp, 6) * -1;
    
    temp = AC(R(0, 0xC0) | R(1, 0xC0) >> 2 | R(2, 0x80) >> 4); 
    gp->rx = EX(temp, 5);

    temp = AC(R(2, 0x1F) << 3);
    gp->ry = EX(temp, 5) * -1;

    temp = (R(2, 0x60) | R(3, 0xE0) >> 3);
    gp->lt = (temp & 0xFC) | temp >> 5;

    temp = (R(3, 0x1F) << 2);
    gp->rt = (temp & 0xFC) | temp >> 5;

    // Finally set the two last bytes as buttons
    gp->buttons = ~(R(4, 0xFF) << 8 | R(5, 0xFF));
}

static inline bool _wiictrl_update_controls(uint32_t ctrlnum) {
    uint32_t i2cid = _WIICTRL_DEF[ctrlnum].i2cid; 

    // Request controls and read 6 bytes
    const uint8_t ctrlreq[] = { 0x00 };
    if (!i2cext_write7(i2cid, WIICTRL_I2C_ADDR, ctrlreq, sizeof(ctrlreq))) return false;
    if (!i2cext_read7(i2cid, WIICTRL_I2C_ADDR, _readbuffer, 6)) return false;

    return true;
}

static inline void _wiictrl_zero_data(struct wiictrl_gamepad* const gp) {
    // Clear memory
    memset(gp, 0, sizeof(struct wiictrl_gamepad));
}

static inline bool _wiictrl_update_and_decode_controls(struct wiictrl_gamepad* const gp, uint32_t ctrlnum) {
    // Execute the update
    if (!_wiictrl_update_controls(ctrlnum)) return false;

    // Get the controller state
    struct wiictrl_curr_state* cst = &_wiictrl_state[ctrlnum];

    // Decode the data
    switch (cst->type) {
    case WIICTRL_TYPE_WII_CLASSIC:
    case WIICTRL_TYPE_WII_CLASSIC_PRO:
        // Decode the data
        _wiictrl_decode_wiiclassic(gp);
        break;

    default:
        _wiictrl_zero_data(gp);
        break;
    }

    return true;
}

static bool _wiictrl_is_connected_detect(uint32_t ctrlnum) {
    // Get a pointer to the controller definition
    const struct wiictrl_def* ctldef = &_WIICTRL_DEF[ctrlnum]; 

    // Get pointer to state
    struct wiictrl_curr_state* cst = &_wiictrl_state[ctrlnum];

    // Determine what the current detect state is
    bool currdet = (gpio_get(ctldef->detect.port, ctldef->detect.pin) != 0);

    // If the current state differs, handle debounce
    if (currdet != cst->detect_state) {
        // The state has changed. We need to do the counting.
        if (cst->detect_cnt < WIICTRL_DETECT_DEBOUNCE) {
            // Increase the counter, and return the state we know
            cst->detect_cnt++;
            return cst->detect_state;
        }
    }

    // If the state is the same, or the debounce count is reached, update the state and clear the counter
    cst->detect_cnt = 0;
    cst->detect_state = currdet;

    // Return the new detect state
    return cst->detect_state;
}

static inline bool _wiictrl_status_changed(struct wiictrl_curr_state* cst, enum wiictrl_status status) {
    if (cst->status == status) return false;
    cst->status = status;
    //cst->iter_cnt = _WIICTRL_COUNTS[status];
    return true;
}

// ==== API Exposed functions ===

void wiictrl_init(void) {
    // Enable clock for both I2C peripherals.
    rcc_periph_clock_enable(RCC_I2C1);
    rcc_periph_clock_enable(RCC_I2C2);

    // Set up clock for GPIOs used for detection
    rcc_periph_clock_enable(RCC_GPIOB);
    rcc_periph_clock_enable(RCC_GPIOC);

    // Enable clock for Alternate Function IO
    rcc_periph_clock_enable(RCC_AFIO);

    // Do remapping of I2C1
    gpio_primary_remap(AFIO_MAPR_SWJ_CFG_FULL_SWJ, AFIO_MAPR_I2C1_REMAP);

    // Enable alternate function for respective I2C GPIOs
    gpio_set_mode(GPIOB, GPIO_MODE_OUTPUT_2_MHZ, GPIO_CNF_OUTPUT_ALTFN_OPENDRAIN, GPIO8 | GPIO9 | GPIO10 | GPIO11);

    // Set up the Detect Pins and I2C settings
    for (uint32_t i = 0; i < WIICTRL_COUNT; i++) {
        // Set up controller port GPIO
        _wiictrl_init_gpio(i);

        // Make sure the i2c is in a reset state initially
        _wiictrl_disable_reset_i2c(i);

        // Make sure all types are set to unknown initially
        _wiictrl_state[i].type = WIICTRL_TYPE_UNKNOWN;
        _wiictrl_state[i].status = WIICTRL_STATUS_DISCONNECTED;
        _wiictrl_state[i].fail_count = 0;
        //_wiictrl_state[i].iter_cnt = 0;
        _wiictrl_state[i].detect_cnt = 0;
        _wiictrl_state[i].detect_state = false;
    }
}


enum wiictrl_status wiictrl_update_info(struct wiictrl_gamepad* const gp, uint32_t ctrlnum) {
    // Check that the controller number is valid
    if (ctrlnum >= WIICTRL_COUNT) return WIICTRL_STATUS_DISCONNECTED;

    // Get pointer to state
    struct wiictrl_curr_state* cst = &_wiictrl_state[ctrlnum];

    // Check if anything is connected to the port. This is already debounced.
    if (!_wiictrl_is_connected_detect(ctrlnum)) {
        // If the state was not already disconnected, set up disconnected state
        if (_wiictrl_status_changed(cst, WIICTRL_STATUS_DISCONNECTED)) {
            // Disable the i2c and reset the peripheral
            _wiictrl_disable_reset_i2c(ctrlnum);
        }

        // Zero the controller data
        _wiictrl_zero_data(gp);

        // Return that we are disconnected
        return WIICTRL_STATUS_DISCONNECTED;
    }

    // If the iteration count is higher than 0, decrease it
    //if (cst->iter_cnt == 0) {
    
    // Something is connected. State machine descides what we do.
    switch (cst->status) {
    // State transitions from disconnected to something connected
    case WIICTRL_STATUS_DISCONNECTED:
        // Zero controller data
        _wiictrl_zero_data(gp);

        // Initialize the I2C port
        _wiictrl_init_enable_i2c(ctrlnum);
        _wiictrl_status_changed(cst, WIICTRL_STATUS_UNIDENTIFIED);
        break;

    // If the state is unidentified, detect the controller type
    case WIICTRL_STATUS_UNIDENTIFIED:
        // Initialize controller, and check if we where successful
        if (_wiictrl_initialize_controller(ctrlnum)) {
            // Set status to connected
            _wiictrl_status_changed(cst, WIICTRL_STATUS_CONNECTED);
        }
        else {
            // Set status to failed
            _wiictrl_status_changed(cst, WIICTRL_STATUS_FAILED);
        }
        break;

    // If status is failed, reset it to disconnected, in order to retry evertthing
    case WIICTRL_STATUS_FAILED:
        // Report status has changed to disconnected
        _wiictrl_status_changed(cst, WIICTRL_STATUS_DISCONNECTED);
        // Disable the i2c and reset the peripheral
        _wiictrl_disable_reset_i2c(ctrlnum);
        break;

    // When connectes, we simply read the state of the controller
    case WIICTRL_STATUS_CONNECTED:
        // Update the controller state
        if (!_wiictrl_update_and_decode_controls(gp, ctrlnum)) {
            // Update the failure counter
            if (cst->fail_count < WIICTRL_MAX_FAIL_COUNT) {
                // Update the failure counter
                cst->fail_count++;
                break;
            }

            // Set state to unidentified briefly to make it re-initialize.
            cst->status = WIICTRL_STATUS_UNIDENTIFIED;
            //cst->iter_cnt = 0;
        }

        // Reset the fail counter
        cst->fail_count = 0;
        break;
    }
    /*
    }
    else {
        // Decrease the iterator
        cst->iter_cnt--;
    }*/
    
    return cst->status;

}
