/**
 * scheduler.c
 * 
 * Simple but powerful scheduler which can be used to implement 
 * Cooperative multitasking in MCU applications.
 */

#include <stdio.h>
#include <stddef.h>
#include <util/scheduler.h>

/* Structure keeping the schedule */

struct {
    struct schedule_task*    first_task;
    struct schedule_task*    current;
    struct schedule_task**   insert;
    int32_t                  tick;
} schedule;

/* ==== Public API implementation ==== */

void scheduler_init(uint32_t tick) {
    // Initialize pointers and ticks
    schedule.first_task = NULL;
    schedule.current = NULL;
    schedule.tick = tick & INT32_MAX;

    // Point to the place to insert the first task
    schedule.insert = &schedule.first_task;
}

void scheduler_register_task(struct schedule_task* task, schduler_task_function_t task_function) {
    // Set the function to the task
    task->task_function = task_function;
    task->execute_tick = -1;    // Idle
    task->next = NULL;          // Important to set NEXT pointer to NULL!

    // Insert the task
    *schedule.insert = task;
    schedule.insert = &task->next;
}

void schedule_task(struct schedule_task* task, int32_t delay) {
    if (delay >= 0) {
        // Calculate the upcoming execution tick, masked to wrap around
        task->execute_tick = (schedule.tick + delay) & INT32_MAX;
    }
    else {
        task->execute_tick = -1;
    }
}

void scheduler_update_tick(uint32_t tick) {
    // Mask the tick to keep within the range of the execute tick
    schedule.tick = tick & INT32_MAX;
    if (schedule.current != NULL) return;

    // If there are no current task, set it up
    schedule.current = schedule.first_task;
}

bool scheduler_execute(void) {
    // If there is no current task, return True
    if (schedule.current == NULL) return true;

    // Get pointer to current scheduled task
    struct schedule_task* curr = schedule.current;

    // Skip task if tick is negative
    if (curr->execute_tick >= 0) {
        // If task function is null, disable the task and skip it
        if (curr->task_function != NULL) {
            // Calculate if tick has passed
            int32_t diff = (schedule.tick - curr->execute_tick) << 1;

            // If the tick counter has passed the set execution time
            if (diff >= 0) {
                // Call the task function and reschedule task
                schedule_task(curr, curr->task_function((uint32_t)schedule.tick));
            }
        }
        else {
            curr->execute_tick = -1;
        }
    }

    // Set up the next task to execute
    schedule.current = curr->next;

    // Retrurn True if the current is NULL
    return schedule.current == NULL;
}
