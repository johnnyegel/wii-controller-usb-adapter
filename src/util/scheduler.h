/**
 * scheduler.h
 * 
 * Simple but powerful scheduler which can be used to implement 
 * Cooperative multitasking in MCU applications.
 */

#ifndef _UTIL_SCHEDULER_H
#define _UTIL_SCHEDULER_H

#include <stdint.h>
#include <stdbool.h>


/**
 * Function called by scheduler to execute task.
 * 
 * The current time (as seen from the scheduler) is provided.
 * The task function should return :
 * - A positive number indicating how many ticks in the future the task should be re-executed at
 * - Zero, if the task should be re-executed once more as soon as possible
 * - A negative number to exclude the task from the schedule.
 * 
 * @param current_tick The current tick as seen from the scheduler
 * @return Number of ticks in the future to execute the task again, or negative number to exclude from schedule
 */
typedef int32_t (*schduler_task_function_t)(uint32_t current_tick);

/**
 * Defines the schedule structure.
 */
struct schedule_task {
    struct schedule_task* next;
    schduler_task_function_t task_function;
    int32_t execute_tick;
};


/**
 * Initializes the scheduler
 */
void scheduler_init(uint32_t tick);

/**
 * Register a given task in the scheduler. 
 * 
 * The task is initially set to idle, and must be scheduled using the schedule_task function.
 * 
 * A task MUST NOT be registered more than once!
 * 
 * @param task The task to register in scheduler
 * @param task_function The task function to call for task execution
 */
void scheduler_register_task(struct schedule_task* task, schduler_task_function_t task_function);

/**
 * Changes the schedule for a Task.
 * 
 * Set the delay to:
 * - Zero, to have the task execute as soon as possible
 * - A positive number to execute the task that many ticks in the future
 *      NOTE! The maximum delay allowed is INT32_MAX / 2. Setting a higher value
 *            will cause inpredictable behaviour.s
 * - A negative number to prevent the task from executing.
 * 
 * @param task The task to schedule
 * @param delay Delay describing what to do
 */
void schedule_task(struct schedule_task* task, int32_t delay);

/**
 * Updates the tick of the scheduler and prepares for execution.
 * 
 * This is typically called when scheduler_execute returns true.
 * 
 * @param tick The new Tick value to set
 */
void scheduler_update_tick(uint32_t tick);

/**
 * Executes the next scheduled task (if it exists) in line.
 * 
 * The function returns true of all tasks in the scheduler has been evaluated for the current pass.
 * You need to call scheduler_update_tick() (regardless if the tick has changed or not) to make the scheduler
 * do another pass.
 * 
 * @return True when all registered tasks have been evaluated. 
 */
bool scheduler_execute(void);

#endif