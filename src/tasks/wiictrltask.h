/**
 * wiictrltask.h - Task for reading the state of the wii controller
 */

#ifndef _TASK_WIICTRLTASK_H
#define _TASK_WIICTRLTASK_H

#include <stdint.h>

#define WIICTRLTASK_REFRESH_RATE    100



/**
 * Initializes the WII controller task
 */
void wiictrltask_init(void);

#endif