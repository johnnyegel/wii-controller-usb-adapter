/**
 * testtask.h
 * 
 * Task that simply blinks the LED
 */

#ifndef _TASK_TESTTASK_H
#define _TASK_TESTTASK_H

void testtask_init(void);

#endif