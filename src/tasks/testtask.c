/**
 * testtask.c
 * 
 * Task that simply blinks the LED
 */

#include <stdio.h>

#include <tasks/testtask.h>
#include <util/scheduler.h>
#include <driver/leds.h>
#include <driver/gpioext.h>

#define BTNPORT     GPIOB
#define BTNPIN      GPIO13

// The task
static struct schedule_task _testtask;


static int32_t _testtask_runner(uint32_t tick) {
    (void)tick;
    /*
    // Initially make sure the led wraps to the first
    static enum led_id led = LED_INTERNAL + 2;
    static enum led_id led_last = LED_INTERNAL + 2;

    // Turn off last led we switched ON
    led_off(led_last);

    // Get the button state
    uint32_t ledadd = !gpio_get(BTNPORT, BTNPIN) ? 2 : 0;

    // Cycle leds
    led += 1;
    if (led > LED_INTERNAL + 2) led = (LED_INTERNAL + 1);

    led_last = led + ledadd;
    led_on(led_last);
    */
    // Toggle the internal LED on and off, and request rerun after 500 ticks
    //led_toggle(LED_INTERNAL);

    return 500;
}

void testtask_init(void) {
    scheduler_register_task(&_testtask, _testtask_runner);
    schedule_task(&_testtask, 500);

    // Set up the button
    /*
    const uint8_t mode = GPIO_MODE_INPUT;
    const uint8_t conf = GPIO_CNF_INPUT_PULL_UPDOWN;
    gpio_set_mode(BTNPORT, mode, conf, BTNPIN);
    gpio_set(BTNPORT, BTNPIN);
    */
}
