/**
 * console.c
 * 
 * Implements a Command Line Console on the CDCACM
 */

#include <tasks/console.h>
#include <util/scheduler.h>
#include <usb/cdc-serial.h>
#include <usb/hid-gamepad.h>

#include <driver/bootloader.h>
#include <driver/leds.h>

#include <stdio.h>
#include <string.h>

// Newline chars to use
#define NEWL     "\r\n"

struct schedule_task _console_task;
struct schedule_task _command_exec_task;

// The commands available
static const char* COMMANDS[] = {
    "nop", "about", "dfu", "help", "dump"
};

enum cli_cmd {
    CLICMD_NOP = 0,
    CLICMD_ABOUT,
    CLICMD_DFU,
    CLICMD_HELP,
    CLICMD_DUMP,
    CLICMD_UNKNOWN      /* Last command indicates unknown */
};

/* ==== Internal State ===*/

static char _cli_cmd_buffer[128];
static uint32_t _cli_pos = 0;
static uint32_t _cli_len = 0;

/* ==== Internal Utility functions ===*/

static inline void _console_cli_prompt(void) {
    cdcacm_send_string("\rcli> ");
    cdcacm_flush();
}

static inline void _console_cli_reset_input(void) {
    // Clear the buffer, and execute command
    _cli_pos = 0;
    _cli_len = 0;
    schedule_task(&_command_exec_task, 0);
}

static void _console_cli_cmd_udpate(void) {
    // Calculate the length
    uint8_t len = _cli_len - _cli_pos;

    // Print out the update
    cdcacm_send_string("\e[K");
    cdcacm_send_data((uint8_t*)(_cli_cmd_buffer + _cli_pos), len);

    if (len > 1) {
        char adjstr[7];
        sprintf(adjstr, "\e[%uD", (len - 1));
        cdcacm_send_string(adjstr);
    }
    cdcacm_flush();
}

static inline void _console_cli_buffer_insert(char c) {
    if (_cli_len >= sizeof(_cli_cmd_buffer) - 1) return;
    for (uint32_t i = _cli_len; i > _cli_pos; i--) {
        _cli_cmd_buffer[i] = _cli_cmd_buffer[i - 1];
    }
    _cli_cmd_buffer[_cli_pos] = c;
    _cli_len++;
    _console_cli_cmd_udpate();
    _cli_pos ++;
}

static inline void _console_cli_buffer_delete(bool is_back_space) {
    if (is_back_space) {
        // Backspace on beginning of buffer does not have an effect
        if (_cli_pos == 0) return;
        _cli_pos--;
        cdcacm_send_string("\x08");
    }
    // Then do the delete
    if (_cli_len == 0) return;
    for (uint32_t i = _cli_pos; i < _cli_len; i++) {
        _cli_cmd_buffer[i] = _cli_cmd_buffer[i + 1];
    }
    // Update the display
    _console_cli_cmd_udpate();
    if (_cli_pos < _cli_len) _cli_len --;
}

static void _console_cli_buffer_cursor(bool is_back) {
    if (is_back) {
        if (_cli_pos == 0) return;
        _cli_pos--;
        cdcacm_send_string("\e[D");
    }
    else {
        if (_cli_pos == _cli_len) return;
        _cli_pos++;
        cdcacm_send_string("\e[C");
    }
}

/* ==== Internal Callback functions ===*/

static void _console_receive_callback(size_t len) {
    // If we receive a zero length, output prompt
    if (len <= 0) {
        strcpy(_cli_cmd_buffer, "about");
        _cli_len = 5;
        schedule_task(&_command_exec_task, 0);
    }
    else {
        // Otherwise, process the input, and schedule the task
        schedule_task(&_console_task, 0);
    }
}

static int32_t _console_execute_task_handler(uint32_t tick) {
    (void)tick;

    // Terminate the string in command buffer
    _cli_cmd_buffer[_cli_len] = '\0';

    // Send a newline, and echo the command
    //cdcacm_send_string(NEWL);
    //cdcacm_send_string(_cli_cmd_buffer);

    // Iterate through commands
    enum cli_cmd cmd = CLICMD_NOP;
    if (_cli_len > 0) {
        const char** ptr = COMMANDS;
        while (cmd < CLICMD_UNKNOWN) {
            // If the command match, we have found it
            if (strncmp(*ptr, _cli_cmd_buffer, strlen(*ptr)) == 0) break;
            ptr ++;
            cmd ++;
        }
    } 

    cdcacm_send_string(NEWL);

    switch (cmd) {
    case CLICMD_NOP: break;
    case CLICMD_ABOUT: cdcacm_send_string("Wii Classic Controller Adapter V1.0"); break;
    case CLICMD_DFU: bootloader_reset_and_enter(); break;
    case CLICMD_DUMP: hidgp_dump_states(); break;
    case CLICMD_UNKNOWN:
        cdcacm_send_string("? Unknown command (try 'help')"); break;
        break;
    
    default:
        cdcacm_send_string("? Command not yet implemented"); 
        break;
    }    
    cdcacm_send_string(NEWL);

    // Reset command buffer
    _cli_pos = 0;
    _cli_len = 0;

    // Display the prompt
    _console_cli_prompt();

    return -1;
}


static int32_t _console_task_handler(uint32_t tick) {
    (void)tick;

    // Keep track of parser state
    static bool _is_escape = false;

    // Determine how many chars we have to process. 
    int len = cdcacm_recv_avail();
    if (len > 8) len = 8;

    while (len--) {
        // Get a byte. If negative, break out of here
        int curr = cdcacm_recv_byte();
        if (curr < 0) break;

        // Cast to a valid ascii char
        char cc = (char)(curr & 0x7F);
        //printf("Char: 0x%02X" NEWL, cc);
        
        // Parse Escape sequences
        if (_is_escape) {
            // If the current char is not a [, it is NOT an escape sequence
            _is_escape = false;
            if (cc == '[') {
                while (len--) {
                    curr = cdcacm_recv_byte();
                    if (curr < 0) {
                        len = -1;
                        break;
                    }
                    cc = (char)(curr & 0x7F);

                    if ((cc >= 'A' && cc <= 'Z') || (cc >= 'a' && cc <= 'c')) {
                        // Last char in escape sequence
                        if (cc == 'D') {
                            _console_cli_buffer_cursor(true);
                        }
                        else{
                            _console_cli_buffer_cursor(false);
                        }
                    }
                    else if (cc == '~') {
                        // Handle delete button
                        _console_cli_buffer_delete(false);
                    }
                }
                if (len >= 0) continue;
                if (len < 0) break;
            }
            else {
                // Simply do a cli reset
                _console_cli_reset_input();
                break;
            }
        }

        switch(cc) {        
        case '\r':
            // If return, schedule command execution
            schedule_task(&_command_exec_task, 0);
            break;

        case '\x03':
            // Simply do a cli reset
            _console_cli_reset_input();
            break;

        case '\e':
            // Possible escape sequence.
            if (len == 0) {
                _console_cli_reset_input();
            }
            else {
                _is_escape = true;
            }
            break;

        case '\x7F':
            // Backspace, reduce the length of command
            _console_cli_buffer_delete(true);
            break;

        default:
            // Make sure it's a printable char
            if (cc >= 0x20 && cc < 0x7F) {
                _console_cli_buffer_insert(cc);
            }
            /*
            else {
                printf("Char: 0x%02X" NEWL, cc);
            } */      
        }
    }

    /*

    // CHARs to handle:
    // - 0x0D - Enter
    // - 0x7F - Backspace
    // - 1B 5B 4X (X: 1-up, 2-down, 3-right, 4-left) 
    // - 1B 5B 32 7E
    */
    return -1;
}

/* === Public API ===*/

void console_init(void) {
    // Register the console task.
    scheduler_register_task(&_console_task, _console_task_handler);
    scheduler_register_task(&_command_exec_task, _console_execute_task_handler);
    cdcacm_set_recv_callback(_console_receive_callback);
}