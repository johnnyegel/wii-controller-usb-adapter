/**
 * console.h
 * 
 * Implements a Command Line Console on the CDCACM
 */

#ifndef _TASK_CONSOLE_H
#define _TASK_CONSOLE_H


/**
 * Initializes the console task
 */ 
void console_init(void);


#endif
