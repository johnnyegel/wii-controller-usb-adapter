/**
 * wiictrltask.h - Task for reading the state of the wii controller
 */

#include <tasks/wiictrltask.h>
#include <driver/wiictrl.h>
#include <driver/leds.h>
#include <usb/hid-gamepad.h>
#include <util/scheduler.h>

// Delay needed for initialization
#define WIICTRL_TASK_INTIDELAY  500
#define WIICTRL_TASK_INTERVAL   5

#define WIICTRL_LED_RED(n)      ((n << 1) + 1)
#define WIICTRL_LED_GRN(n)      ((n << 1) + 2)


static inline void _wiictrl_set_led(uint32_t ctrnum, enum wiictrl_status status) {
    static enum wiictrl_status laststatus[WIICTRL_COUNT];
    
    // Don't do any changes unless there is a status change
    if (status == laststatus[ctrnum]) return;

    // Update our state
    laststatus[ctrnum] = status;

    // Calculate the base LED for the controller
    uint32_t baseled = (ctrnum * 2) + 1;

    // Calculate the LED number
    switch (status) {
        case WIICTRL_STATUS_UNIDENTIFIED:
            led_on(baseled);        // RED
            led_off(baseled + 1);   // GREEN
            break;
        
        case WIICTRL_STATUS_CONNECTED:
            led_off(baseled);        // RED
            led_on(baseled + 1);   // GREEN
            break;

        default:
            led_off(baseled);        // RED
            led_off(baseled + 1);   // GREEN
            break;
    }
}

static int32_t _wiictrl_update_handler(uint32_t tick) {
    (void)tick;
    // Keep track of the device to poll
    static int ix = 0;

    // Set the LED accordingly
    enum wiictrl_status status = wiictrl_update_info(hid_gamepads[ix], ix);

    // Unless the status is connected, we have nothing more to do
    if (status == WIICTRL_STATUS_CONNECTED) {
        // Send data to the USB host
        //bool is_fault = 
        hidgp_gamepad_state_udpated(ix);
        // Change the status periodically to indicate a fault
        /* XXX: No blinking here
        if (is_fault && (tick & 0x80)) {
            status = WIICTL_STATUS_NOT_CONNECTED;
        } */
    }

    // Make sure the LEDs are up to date
    _wiictrl_set_led(ix, status);
    
    // Toggle the controller id
    ix = 1 - ix;

    return WIICTRL_TASK_INTERVAL;
}

static struct schedule_task _wiictrl_update_task;

void wiictrltask_init(void) {  
    scheduler_register_task(&_wiictrl_update_task, _wiictrl_update_handler);
    // Schedule delayed start.
    schedule_task(&_wiictrl_update_task, WIICTRL_TASK_INTIDELAY);
}

