/**
 * main.c
 * 
 * Entrypoint for the C application for the Wii Controller Adapter project.
 * 
 */

#include <libopencm3/stm32/rcc.h>
#include <system/systime.h>
#include <system/system_core.h>
#include <driver/leds.h>
#include <driver/wiictrl.h>
#include <usb/wcausb.h>
#include <usb/cdc-serial.h>
#include <util/scheduler.h>

#include <stdio.h>

#include <tasks/console.h>
#include <tasks/wiictrltask.h>


#define TICKOFFS    (INT32_MAX - 20000)

static void _init_lowlevel(void) {
    // Initialize core
    system_core_init();

    // Set up clock to HSE @ 8Mhz
    rcc_clock_setup_in_hse_8mhz_out_72mhz();

    // Initialize the systick with priority of 0x80
    systime_init(0x80);
}

static void _init_drivers(void) {
    // Initialize LEDS
    leds_init();

    // Initialize usb, with IRQ driven polling 
    // (which is correct, as the poll function is basically an interrupt state handler)
    wcausb_init(true);

    // Initialize the WII Controller driver
    wiictrl_init();
}

static void _init_tasks(void) {
    // Initialize the scheduler
    scheduler_init(0);

    // Initialize tasks
    console_init();

    // Initialize the WII controller task
    wiictrltask_init();
}

static void _main_init(void) {
    _init_lowlevel();
    _init_drivers();
    _init_tasks();
}

/**
 * Entry point function
 */
int main(void) {
    // Initialize main stuff
    _main_init();

    // Loop forever
    while (1) {
        // Simply run the scheduler
        if (scheduler_execute()) {
            // Update the tick when no more tasks are left
            scheduler_update_tick(systime_now);
        }
    }

    return 0;
}