/**
 * usb/cdc-serial.c
 * 
 * Virtual COM port definition
 */

#include <usb/wcausb.h>
#include <usb/cdc-serial.h>
#include <util/CBUF.h>
#include <libopencm3/usb/cdc.h>

#include <stddef.h>

/* The Interface number for the CDC control interface */
#define USBCDC_IFNUM                2
#define USBCDC_TEXT_IX              4

/* Serial ACM interface */
#define CDCACM_PACKET_SIZE 	    64      /* 64 is the MAX!!!! */
#define CDCACM_UART_ENDPOINT	0x03
#define CDCACM_INTR_ENDPOINT	0x84

/* Define buffers for TX and RX */

#define CDCADM_RX_BUFFERSIZE    (1 << 7)
#define CDCADM_TX_BUFFERSIZE    (1 << 9)


// Receive buffer
static struct {
	volatile	uint16_t	m_get_idx;
	volatile	uint16_t	m_put_idx;
				uint8_t		m_entry[CDCADM_RX_BUFFERSIZE];
} cdcacm_rx_buf;

// Transmit buffer
static struct {
	volatile	uint16_t	m_get_idx;
	volatile	uint16_t	m_put_idx;
				uint8_t		m_entry[CDCADM_TX_BUFFERSIZE];
} cdcacm_tx_buf;

// Keep track if we need to flush
static bool   	        _cdcacm_need_empty_tx = false;
static bool   	        _cdcacm_flush_requested = false;

// Keep pointer to configured device
static cdcacm_recv_callback_t _cdcacm_recv_callback = NULL;


/* USB Descriptors definitions */

static const struct usb_endpoint_descriptor uart_comm_endp[] = {{
	.bLength = USB_DT_ENDPOINT_SIZE,
	.bDescriptorType = USB_DT_ENDPOINT,
	.bEndpointAddress = CDCACM_INTR_ENDPOINT,
	.bmAttributes = USB_ENDPOINT_ATTR_INTERRUPT,
	.wMaxPacketSize = 16,
	.bInterval = 255,
}};

static const struct usb_endpoint_descriptor uart_data_endp[] = {{
	.bLength = USB_DT_ENDPOINT_SIZE,
	.bDescriptorType = USB_DT_ENDPOINT,
	.bEndpointAddress = CDCACM_UART_ENDPOINT,
	.bmAttributes = USB_ENDPOINT_ATTR_BULK,
	.wMaxPacketSize = CDCACM_PACKET_SIZE,
	.bInterval = 1,
}, {
	.bLength = USB_DT_ENDPOINT_SIZE,
	.bDescriptorType = USB_DT_ENDPOINT,
	.bEndpointAddress = 0x80 | CDCACM_UART_ENDPOINT,
	.bmAttributes = USB_ENDPOINT_ATTR_BULK,
	.wMaxPacketSize = CDCACM_PACKET_SIZE,
	.bInterval = 1,
}};

static const struct {
	struct usb_cdc_header_descriptor cdc_header;
	struct usb_cdc_call_management_descriptor call_mgmt;
	struct usb_cdc_acm_descriptor acm;
	struct usb_cdc_union_descriptor cdc_union;
} __attribute__((packed)) uart_cdcacm_functional_descriptors = {
	.cdc_header = {
		.bFunctionLength = sizeof(struct usb_cdc_header_descriptor),
		.bDescriptorType = CS_INTERFACE,
		.bDescriptorSubtype = USB_CDC_TYPE_HEADER,
		.bcdCDC = 0x0110,
	},
	.call_mgmt = {
		.bFunctionLength = sizeof(struct usb_cdc_call_management_descriptor),
		.bDescriptorType = CS_INTERFACE,
		.bDescriptorSubtype = USB_CDC_TYPE_CALL_MANAGEMENT,
		.bmCapabilities = 0,
		.bDataInterface = (USBCDC_IFNUM + 1),
	},
	.acm = {
		.bFunctionLength = sizeof(struct usb_cdc_acm_descriptor),
		.bDescriptorType = CS_INTERFACE,
		.bDescriptorSubtype = USB_CDC_TYPE_ACM,
		.bmCapabilities = 2, /* SET_LINE_CODING supported*/
	},
	.cdc_union = {
		.bFunctionLength = sizeof(struct usb_cdc_union_descriptor),
		.bDescriptorType = CS_INTERFACE,
		.bDescriptorSubtype = USB_CDC_TYPE_UNION,
		.bControlInterface = (USBCDC_IFNUM + 0),
		.bSubordinateInterface0 = (USBCDC_IFNUM + 1),
	 }
};

const struct usb_interface_descriptor cdc_comm_iface = {
	.bLength = USB_DT_INTERFACE_SIZE,
	.bDescriptorType = USB_DT_INTERFACE,
	.bInterfaceNumber = (USBCDC_IFNUM + 0),
	.bAlternateSetting = 0,
	.bNumEndpoints = 1,
	.bInterfaceClass = USB_CLASS_CDC,
	.bInterfaceSubClass = USB_CDC_SUBCLASS_ACM,
	.bInterfaceProtocol = USB_CDC_PROTOCOL_AT,
	.iInterface = USBCDC_TEXT_IX,

	.endpoint = uart_comm_endp,

	.extra = &uart_cdcacm_functional_descriptors,
	.extralen = sizeof(uart_cdcacm_functional_descriptors)
};

const struct usb_interface_descriptor cdc_data_iface = {
	.bLength = USB_DT_INTERFACE_SIZE,
	.bDescriptorType = USB_DT_INTERFACE,
	.bInterfaceNumber = (USBCDC_IFNUM + 1),
	.bAlternateSetting = 0,
	.bNumEndpoints = 2,
	.bInterfaceClass = USB_CLASS_DATA,
	.bInterfaceSubClass = 0,
	.bInterfaceProtocol = 0,
	.iInterface = 0,

	.endpoint = uart_data_endp,
};

const struct usb_iface_assoc_descriptor cdc_iface_assoc = {
	.bLength = USB_DT_INTERFACE_ASSOCIATION_SIZE,
	.bDescriptorType = USB_DT_INTERFACE_ASSOCIATION,
	.bFirstInterface = (USBCDC_IFNUM + 0),
	.bInterfaceCount = 2,
	.bFunctionClass = USB_CLASS_CDC,
	.bFunctionSubClass = USB_CDC_SUBCLASS_ACM,
	.bFunctionProtocol = USB_CDC_PROTOCOL_AT,
	.iFunction = 0,
};

/* ===== Internal functions ===== */

static void _cdcacm_report_received(void) {
    if (_cdcacm_recv_callback == NULL) return;
    size_t recv_count = cdcacm_recv_avail();
    if (recv_count > 0) _cdcacm_recv_callback(recv_count);    
}

static void _cdcacm_usb_out_cb(usbd_device *dev, uint8_t ep) {
	uint16_t len;

    // Check if there is more than 64 bytes available continuously in the buffer
    uint16_t space = CBUF_ContigSpace(cdcacm_rx_buf);
	if (space >= 64) {
		// We can read directly into our buffer
		len = usbd_ep_read_packet(dev, ep, 
								  CBUF_GetPushEntryPtr(cdcacm_rx_buf), 64);
		CBUF_AdvancePushIdxBy(cdcacm_rx_buf, len);
	} else {
		// Do it character by character. This covers 2 situations:
		// 1 - We're near the end of the buffer (so free space isn't contiguous)
		// 2 - We don't have much free space left.
		char buf[64];
		len = usbd_ep_read_packet(dev, ep, buf, 64);
		for (int i = 0; i < len; i++) {
			// If the Rx buffer fills, then we drop the new data.
			if (CBUF_IsFull(cdcacm_rx_buf)) {				
                break;
			}
			CBUF_Push(cdcacm_rx_buf, buf[i]);
		}
	}

    // Make sure any listeners are notified
    _cdcacm_report_received();
}


static void _cdcacm_set_modem_state(usbd_device *dev, int iface, bool dsr, bool dcd)
{
	char buf[10];
	struct usb_cdc_notification *notif = (void*)buf;
	/* We echo signals back to host as notification */
	notif->bmRequestType = 0xA1;
	notif->bNotification = USB_CDC_NOTIFY_SERIAL_STATE;
	notif->wValue = 0;
	notif->wIndex = iface;
	notif->wLength = 2;
	buf[8] = (dsr ? 2 : 0) | (dcd ? 1 : 0);
	buf[9] = 0;

    usbext_ep_write_packet(dev, 0x82 + iface, buf, 10);
}

static enum usbd_request_return_codes _cdcacm_control_request_callback(
		usbd_device *usbd_dev,
		struct usb_setup_data *req, uint8_t **buf, uint16_t *len,
		usbd_control_complete_callback *complete) 
{
	(void)complete;
	(void)buf;

	switch(req->bRequest) {
	case USB_CDC_REQ_SET_CONTROL_LINE_STATE: {
		_cdcacm_set_modem_state(usbd_dev, req->wIndex, true, true);
        // Call callback with a zero value to indicate that a connection happened.
        if (req->wValue & 1 && _cdcacm_recv_callback != NULL) {
            _cdcacm_recv_callback(0);
        }
		return USBD_REQ_HANDLED;
    }

	case USB_CDC_REQ_SET_LINE_CODING:
		if(*len < sizeof(struct usb_cdc_line_coding)) {
			return USBD_REQ_NOTSUPP;
        }

		return USBD_REQ_HANDLED;
	}
    return USBD_REQ_NEXT_CALLBACK;
	//return USBD_REQ_NOTSUPP;
}

/* ===== Exported API ===== */

void cdcacm_set_usb_config(usbd_device *dev, uint16_t wValue) {
	(void) wValue;

	/* Serial interface */
	usbd_ep_setup(dev, 0x00 | CDCACM_UART_ENDPOINT, USB_ENDPOINT_ATTR_BULK,
	              CDCACM_PACKET_SIZE, _cdcacm_usb_out_cb);
	usbd_ep_setup(dev, 0x80 | CDCACM_UART_ENDPOINT, USB_ENDPOINT_ATTR_BULK,
	              CDCACM_PACKET_SIZE, NULL);
	usbd_ep_setup(dev, CDCACM_INTR_ENDPOINT, USB_ENDPOINT_ATTR_INTERRUPT, 16, NULL);

	usbd_register_control_callback(dev,
			USB_REQ_TYPE_CLASS | USB_REQ_TYPE_INTERFACE,
			USB_REQ_TYPE_TYPE | USB_REQ_TYPE_RECIPIENT,
			_cdcacm_control_request_callback);

	/* Notify the host that DCD is asserted.
	 * Allows the use of /dev/tty* devices on *BSD/MacOS
	 */
	_cdcacm_set_modem_state(dev, 2, true, true);
}

void cdcacm_transmit_txbuffer(usbd_device *usbd_dev) {
	uint16_t len = CBUF_ContigLen(cdcacm_tx_buf);
    if (len == 0) {
        // Nothing to do unless we are explicitly asked to push empty packet
        if (!_cdcacm_need_empty_tx && !_cdcacm_flush_requested) return;
        // We can set this to false, as the packet will now be sent
        _cdcacm_flush_requested = false;
    }
	else if (len > 64) {
		len = 64;
	}
    // Get pointer to the continuous section of the buffer
	uint8_t *pop_ptr = CBUF_GetPopEntryPtr(cdcacm_tx_buf);
    uint16_t sent = usbext_ep_write_packet(usbd_dev, 0x80 | CDCACM_UART_ENDPOINT, pop_ptr, len);

	// If we just sent a packet of 64 bytes. If we get called again and
	// there is no more data to send, then we need to send a zero byte
	// packet to indicate to the host to release the data it has buffered.
	_cdcacm_need_empty_tx = (sent == 64);
    //_cdcacm_need_empty_tx = (sent > 0);
    
	CBUF_AdvancePopIdxBy(cdcacm_tx_buf, sent);
}

void cdcacm_set_recv_callback(cdcacm_recv_callback_t callback) {
    _cdcacm_recv_callback = callback;
    _cdcacm_report_received();
}

size_t cdcacm_recv_avail(void) {
	return CBUF_Len(cdcacm_rx_buf);
}

size_t cdcacm_send_avail(void) {
	return CDCADM_TX_BUFFERSIZE - CBUF_Len(cdcacm_tx_buf);
}

int cdcacm_recv_byte(void) {
	if (CBUF_IsEmpty(cdcacm_rx_buf)) {
		return -1;
	}
	return CBUF_Pop(cdcacm_rx_buf);
}

size_t cdcacm_recv_data(uint8_t* target, size_t maxlen) {
    // TODO: Implement more efficient using chunk memcpy!
    size_t cnt = 0;
    while (maxlen && !CBUF_IsEmpty(cdcacm_rx_buf)) {
        *target++ = CBUF_Pop(cdcacm_rx_buf);
        cnt++;
        maxlen--;
    }
    return cnt;
}

bool cdcacm_send_byte(uint8_t ch) {
	if (!CBUF_IsFull(cdcacm_tx_buf)) {
		CBUF_Push(cdcacm_tx_buf, ch);
		return true;
	}
	return false;
}

int cdcacm_send_data(const uint8_t *data, size_t len) {
    // TODO: Implement more efficient using chunk memcpy!
	int cnt = 0;
	for (const uint8_t *end = data + len; data < end; data++) {
		if (cdcacm_send_byte(*data)) cnt++;
		else return cnt;
	}
	return cnt;
}

int cdcacm_send_string(const char *str) {
    // TODO: Implement more efficient using chunk memcpy!
	int cnt = 0;
    while (*str != '\0') {
		if (cdcacm_send_byte(*str)) cnt++;
		else return cnt;
        str ++;
    }
    return cnt;
}

void cdcacm_flush(void) {
    // Set emtpy TX flag, which will cause a flush
    _cdcacm_flush_requested = true;
}
