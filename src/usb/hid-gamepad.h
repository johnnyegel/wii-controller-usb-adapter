/**
 * usb/hid-gamepad.h
 * 
 * UDB HID Gamepad 
 */
#ifndef _USB_HID_GAMEPAD_H
#define _USB_HID_GAMEPAD_H

#include <libopencm3/usb/usbd.h>
#include <driver/wiictrl.h>

// Externalize to include in the complete USB report
extern const struct usb_interface_descriptor hid_gamepad1_iface;
extern const struct usb_interface_descriptor hid_gamepad2_iface;

// Array containing pointers to the Gamepad structures of the HID reports
extern struct wiictrl_gamepad* const hid_gamepads[WIICTRL_COUNT];

/**
 * Callback which sets USB config
 */
void hidgp_set_usb_config(usbd_device *dev, uint16_t wValue);

/**
 * Flag that there is an update to the gamepad state
 */
bool hidgp_gamepad_state_udpated(uint32_t gpnum);

/**
 * Called when it's due to write any scheduled data
 */
bool hidgp_gamepad_transmit_next(usbd_device *dev);


/**
 * Writes the current gamepad states
 */
void hidgp_dump_states(void);

#endif