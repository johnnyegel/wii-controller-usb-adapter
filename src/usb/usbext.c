/**
 * usb/usbext.c
 * 
 * Extensions for the main libopencm3 usb implementation
 */

#include <usb/usbext.h>
#include <libopencm3/cm3/nvic.h>

static bool _usbext_irq_enabled = false;

void usbext_set_irq_poll(bool enable) {
    _usbext_irq_enabled = enable;

    if (enable) {
        nvic_enable_irq(NVIC_USB_LP_CAN_RX0_IRQ);
        nvic_enable_irq(NVIC_USB_WAKEUP_IRQ);
    }
    else {
        nvic_disable_irq(NVIC_USB_LP_CAN_RX0_IRQ);
        nvic_disable_irq(NVIC_USB_WAKEUP_IRQ);
    }
}

uint16_t usbext_ep_write_packet(usbd_device *dev, uint8_t addr, const void *buf, uint16_t len) {
    if (!dev) return 0;
    nvic_disable_irq(NVIC_USB_LP_CAN_RX0_IRQ);

    uint16_t wlen = usbd_ep_write_packet(dev, addr, buf, len);
    if (_usbext_irq_enabled) nvic_enable_irq(NVIC_USB_LP_CAN_RX0_IRQ);

    return wlen;
}