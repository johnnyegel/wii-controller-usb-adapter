/**
 * usb/serial.h
 * 
 * Virtual COM port definition
 */

#ifndef _USB_CDC_SERIAL_H
#define _USB_CDC_SERIAL_H


#include <libopencm3/usb/usbd.h>
#include <usb/usbext.h>

#include <stdbool.h>
#include <stdint.h>
#include <stddef.h>

// Define externals for the Interfaces
extern const struct usb_interface_descriptor cdc_comm_iface;
extern const struct usb_interface_descriptor cdc_data_iface;
extern const struct usb_iface_assoc_descriptor cdc_iface_assoc;

/**
 * Callback function definition, called when data or new connections are received.
 * The count indicates how many bytes are available to read in total. Note that the
 * count will be 0 if a new connection is registered. In this case, a terminal should 
 * output a prompt.
 * 
 * @param count The number of bytes available for reading, or 0 when new connections arrive
 */
typedef void (*cdcacm_recv_callback_t)(size_t count);

/**
 * Callback which sets USB config
 */
void cdcacm_set_usb_config(usbd_device *dev, uint16_t wValue);

/**
 * Called when data in TX buffer should be transmitted to USB
 */
void cdcacm_transmit_txbuffer(usbd_device *usbd_dev);

/**
 * Set callback to call when data is received
 * 
 * This should be used to handle a terminal task, which can subscribe to this callback and only do stuff
 * when it's called. 
 * 
 * @param callback The callback to set
 */
void cdcacm_set_recv_callback(cdcacm_recv_callback_t callback);

/**
 * Returns the number of characters which are currently ready for reception
 * @return Size of available data
 */
size_t cdcacm_recv_avail(void);

/**
 * Returns the number of bytes available for transmission
 * @return Size available in TX buffer
 */
size_t cdcacm_send_avail(void);

/**
 * Receive the next byte in the RX buffer. Returns -1 if there are no bytes in RX buffer
 * @return The next byte, or -1 if there is no more data
 */
int cdcacm_recv_byte(void);

/**
 * Receive data into given target buffer.
 * 
 * The number of bytes received into the target is returned. This will be restricted to maxlen.
 * 
 * @param target The target buffer
 * @param maxlen The maximum datalength to read
 * @return The number of bytes received
 */
size_t cdcacm_recv_data(uint8_t* target, size_t maxlen);

/**
 * Sends a byte to the serial terminal
 * NOTE! If serial is not connected (cdcacm_is_connected() returns false), the data will be
 * buffered and kept until something actually connects and reads the data. If the TX buffer is
 * full, any provided bytes will be silently dropped.
 * 
 * @param ch The byte to send
 */
bool cdcacm_send_byte(uint8_t ch);

/**
 * Sends data to the connected terminal.
 * If nothing is connected, the data is written to the TX buffer, and kept until someone connects.
 * If the buffer is full, data is silently dropped.
 * 
 * @param data Pointer to the data to send
 * @param len The length of the data to send
 */
int cdcacm_send_data(const uint8_t *data, size_t len);

/**
 * Sends a null terminate string to a listening terminal. 
 * 
 * @param str The string to send
 */
int cdcacm_send_string(const char *str);

/**
 * Makes sure the TX data is flushed
 */
void cdcacm_flush(void);

#endif