/**
 * usb/usb.c
 * 
 * Top level API for the USB implementation
 */

#include <stddef.h>

#include <usb/wcausb.h>
#include <usb/cdc-serial.h>
#include <usb/hid-gamepad.h>
#include <util/scheduler.h>
#include <libopencm3/stm32/rcc.h>
#include <libopencm3/cm3/nvic.h>

#define WCAUSB_WRITE_TASK_INTERVAL      1

/* USB device handle */
static usbd_device* _wcausb_dev;


/* TODO: Define all the interfaces */
const struct usb_interface ifaces[] = {
    // HID GamePad 1 Interface
    {
        .num_altsetting = 1,
        .altsetting = &hid_gamepad1_iface
    },
    // HID GamePad 2 Interface
    {
        .num_altsetting = 1,
        .altsetting = &hid_gamepad2_iface   
    },
    // USB UART Control Port 
    {
        .num_altsetting = 1,
        .iface_assoc = &cdc_iface_assoc,
        .altsetting = &cdc_comm_iface  
    },
    // USB UART Data Port 
    {
        .num_altsetting = 1,
        .altsetting = &cdc_data_iface
    },
};

/* Device Descriptor */
const struct usb_device_descriptor dev_descr = {
	.bLength = USB_DT_DEVICE_SIZE,
	.bDescriptorType = USB_DT_DEVICE,
	.bcdUSB = 0x0200,
	.bDeviceClass = 0,
	.bDeviceSubClass = 0,
	.bDeviceProtocol = 0,
	.bMaxPacketSize0 = 64,
	.idVendor = 0x0483,     // ST Micro
	.idProduct = 0x5710,    // Joystick demo
	.bcdDevice = 0x0100,    // Version 1
	.iManufacturer = 1,
	.iProduct = 2,
	.iSerialNumber = 3,
	.bNumConfigurations = 1,
};

/* Device configuration (there is only one) */
const struct usb_config_descriptor config = {
	.bLength = USB_DT_CONFIGURATION_SIZE,
	.bDescriptorType = USB_DT_CONFIGURATION,
	.wTotalLength = 0,
	.bNumInterfaces = sizeof(ifaces) / sizeof(ifaces[0]),
	.bConfigurationValue = 1,
	.iConfiguration = 0,
	.bmAttributes = 0xC0,
	.bMaxPower =  0xC8,     // 400mA  (0xC8 * 2mA)

    .interface = ifaces,
};

static const char * const wcausb_strings[] = { 
    "BizarreDesign",
    "DualWiiClassic",
    "WIICA",
    "Console",
    "Gamepad1",
    "Gamepad2",
 };

/* Buffer to be used for control requests. */
static uint8_t usbd_control_buffer[128];

// Callback to handle SOF frames which the host sends continuously at 1ms intervals
static void _wcausb_handle_next_transmission(void) {
    // Check that there is a device
    if (_wcausb_dev == NULL) return;
    
    // Let the HID known it can send data, and return if if did
    if (hidgp_gamepad_transmit_next(_wcausb_dev)) return;

    // Request the Serial driver to transmit it's buffer, only when HID has nothing to transmit
    cdcacm_transmit_txbuffer(_wcausb_dev);
}

// Callback to set up the USB stuff
static void _wcausb_set_config_callback(usbd_device *dev, uint16_t wValue)
{
	// Configure HID interfaces
    hidgp_set_usb_config(dev, wValue);

    // Configure CDC interface
	cdcacm_set_usb_config(dev, wValue);

    // Register a SOF calback, which is used for sending packets
    usbd_register_sof_callback(_wcausb_dev, _wcausb_handle_next_transmission);
}

// Task handler called by scheduler
/*
static int32_t _wcausb_transmission_task_handler(uint32_t tick) {
    (void) tick;

    // Call the transmission handler
    _wcausb_handle_next_transmission();

    // We want to be called once every millisecond
    return WCAUSB_WRITE_TASK_INTERVAL;
}
*/
/* USB IRQ Handlers */
void usb_lp_can_rx0_isr(void) {
    wcausb_poll();
}
void usb_wakeup_isr(void) {
    wcausb_poll();
}

/* ===== Exported API ===== */

//static struct schedule_task _wiictrl_usb_tx_task;
static bool _enable_irq;

void wcausb_init(bool enable_irq) {
    // Keep the IRQ enable setting
    _enable_irq = enable_irq;

    // Make sure clock for USB is enabled.
    rcc_periph_clock_enable(RCC_OTGFS);
    
    // Initialize the USB device
    _wcausb_dev = usbd_init(&st_usbfs_v1_usb_driver, 
        &dev_descr, &config, wcausb_strings, 6,
		usbd_control_buffer, sizeof(usbd_control_buffer));
	
    // Register configuration callback
    usbd_register_set_config_callback(_wcausb_dev, _wcausb_set_config_callback);
    
    // Enable RX IRQ for USB if requested
    usbext_set_irq_poll(_enable_irq);

    /* XXX: This did not work well...
    // Schedule a task which handles transmission
    scheduler_register_task(&_wiictrl_usb_tx_task, _wcausb_transmission_task_handler);
    schedule_task(&_wiictrl_usb_tx_task, WCAUSB_WRITE_TASK_INTERVAL);
    */
}

void wcausb_poll(void) {
 	if (!_wcausb_dev) return;
    usbd_poll(_wcausb_dev);
}
