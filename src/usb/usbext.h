/**
 * usb/usbext.h
 * 
 * Extensions to the main USB library of Libopencm3, in order to make it more sane to use
 */

#ifndef _USB_USBEXT_H
#define _USB_USBEXT_H

#include <libopencm3/usb/usbd.h>

/**
 * Sets IRQ driven polling
 */
void usbext_set_irq_poll(bool enable);

/**
 * Write a packet on the end-point. This is a wrapper of the API, which insures
 * the Interrupt is disabled while the packet if bein written.
 */ 
uint16_t usbext_ep_write_packet(usbd_device *dev, uint8_t addr, const void *buf, uint16_t len);


#endif
