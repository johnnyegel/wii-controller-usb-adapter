/**
 * usb/hid-gamepad.c
 * 
 * UDB HID Gamepad 
 */

#include <stdio.h>
#include <stddef.h>
#include <string.h>

#include <libopencm3/usb/hid.h>
#include <usb/usbext.h>
#include <usb/hid-gamepad.h>
#include <driver/wiictrl.h>

/* Base interface number */
#define USBHID_IFNUM        0
#define USBHID_EP_BASEADDR  0x81
#define USBHID_TEXT_IX      5

/* Define the size of the report and Gape */
#define USBHID_GP_REPORT_SIZE       sizeof(struct wiictrl_gamepad)
#define USBHID_GP_MAX_SIZE          USBHID_GP_REPORT_SIZE      // Packet MAX size
#define USBHID_GP_INTERVAL          5       // Milliseconds

// Structure used to report HID data
static struct wiictrl_gamepad _gp_reports[WIICTRL_COUNT];

// Set up the pointers, and flags uded to mark updates
struct wiictrl_gamepad* const hid_gamepads[WIICTRL_COUNT] = { &_gp_reports[0], &_gp_reports[1] };
static bool _hidgp_state_updated[WIICTRL_COUNT];
static bool _hidgp_fault_flags[WIICTRL_COUNT] = {0};

// HID Report descriptor
static const uint8_t hid_report_descriptor[] = {
    0x05, 0x01,        // Usage Page (Generic Desktop Ctrls)
    0x09, 0x05,        // Usage (Game Pad)
    0xA1, 0x01,        // Collection (Application)
    0xA1, 0x00,        //   Collection (Physical)
    0x05, 0x09,        //     Usage Page (Button)
    0x19, 0x01,        //     Usage Minimum (0x01)
    0x29, 0x10,        //     Usage Maximum (0x10)
    0x15, 0x00,        //     Logical Minimum (0)
    0x25, 0x01,        //     Logical Maximum (1)
    0x95, 0x10,        //     Report Count (16)
    0x75, 0x01,        //     Report Size (1)
    0x81, 0x02,        //     Input (Data,Var,Abs,No Wrap,Linear,Preferred State,No Null Position)
    0x05, 0x01,        //     Usage Page (Generic Desktop Ctrls)
    0x09, 0x30,        //     Usage (X)
    0x09, 0x31,        //     Usage (Y)
    0x09, 0x34,        //     Usage (Ry)
    0x09, 0x35,        //     Usage (Rz)
    0x09, 0x32,        //     Usage (Z)
    0x09, 0x33,        //     Usage (Rx)
    0x15, 0x81,        //     Logical Minimum (-127)
    0x26, 0x7F, 0x00,  //     Logical Maximum (127)
    0x75, 0x08,        //     Report Size (8)
    0x95, 0x06,        //     Report Count (6)
    0x81, 0x02,        //     Input (Data,Var,Abs,No Wrap,Linear,Preferred State,No Null Position)
    0xC0,              //   End Collection
    0xC0,              // End Collection
};

/* HID Descriptor */
static const struct
{
    struct usb_hid_descriptor hid_descriptor;
    struct
    {
        uint8_t bReportDescriptorType;
        uint16_t wDescriptorLength;
    } __attribute__((packed)) hid_report;
} __attribute__((packed)) hid_function = {
    .hid_descriptor = {
        .bLength = sizeof(hid_function),
        .bDescriptorType = USB_DT_HID,
        .bcdHID = 0x0111,
        .bCountryCode = 0,
        .bNumDescriptors = 1,
    },
    .hid_report = {
        .bReportDescriptorType = USB_HID_DT_REPORT,
        .wDescriptorLength = sizeof(hid_report_descriptor),
    }};

/* Gamepad Endpoints */
static const struct usb_endpoint_descriptor hid_gamepad1_endpoint = {
    .bLength = USB_DT_ENDPOINT_SIZE,
    .bDescriptorType = USB_DT_ENDPOINT,
    .bEndpointAddress = USBHID_EP_BASEADDR + 0,
    .bmAttributes = USB_ENDPOINT_ATTR_INTERRUPT,
    .wMaxPacketSize = USBHID_GP_MAX_SIZE,
    .bInterval = USBHID_GP_INTERVAL,
};
static const struct usb_endpoint_descriptor hid_gamepad2_endpoint = {
    .bLength = USB_DT_ENDPOINT_SIZE,
    .bDescriptorType = USB_DT_ENDPOINT,
    .bEndpointAddress = USBHID_EP_BASEADDR + 1,
    .bmAttributes = USB_ENDPOINT_ATTR_INTERRUPT,
    .wMaxPacketSize = USBHID_GP_MAX_SIZE,
    .bInterval = USBHID_GP_INTERVAL,
};

/* Gamepad Interfaces */
const struct usb_interface_descriptor hid_gamepad1_iface = {
    .bLength = USB_DT_INTERFACE_SIZE,
    .bDescriptorType = USB_DT_INTERFACE,
    .bInterfaceNumber = (USBHID_IFNUM + 0),
    .bAlternateSetting = 0,
    .bNumEndpoints = 1,
    .bInterfaceClass = USB_CLASS_HID,
    .bInterfaceSubClass = 0,
    .bInterfaceProtocol = 0,
    .iInterface = USBHID_TEXT_IX,

    // Add the corresponding endpoint
    .endpoint = &hid_gamepad1_endpoint,

    // Add HID function and length as extra
    .extra = &hid_function,
    .extralen = sizeof(hid_function)
};

const struct usb_interface_descriptor hid_gamepad2_iface = {
    .bLength = USB_DT_INTERFACE_SIZE,
    .bDescriptorType = USB_DT_INTERFACE,
    .bInterfaceNumber = (USBHID_IFNUM + 1),
    .bAlternateSetting = 0,
    .bNumEndpoints = 1,
    .bInterfaceClass = USB_CLASS_HID,
    .bInterfaceSubClass = 0,
    .bInterfaceProtocol = 0,
    .iInterface = USBHID_TEXT_IX + 1,

    // Add the corresponding endpoint
    .endpoint = &hid_gamepad2_endpoint,

    // Add HID function and length as extra
    .extra = &hid_function,
    .extralen = sizeof(hid_function)
};


static enum usbd_request_return_codes _hidgp_control_request(
    usbd_device *usbd_dev,
    struct usb_setup_data *req, uint8_t **buf, uint16_t *len,
    usbd_control_complete_callback *complete)
{
    (void)complete;
    (void)usbd_dev;

    if ((req->bmRequestType != 0x81) ||
        (req->bRequest != USB_REQ_GET_DESCRIPTOR) ||
        (req->wValue != 0x2200)) 
    {
        return USBD_REQ_NEXT_CALLBACK;
    }

    /* Handle the HID report descriptor. */
    *buf = (uint8_t *)hid_report_descriptor;
    *len = sizeof(hid_report_descriptor);

    return USBD_REQ_HANDLED;
}
/*
uint8_t readbuf[USBHID_GP_MAX_SIZE];
uint32_t readlen;

static void _hidgp_usb_out_cb(usbd_device *dev, uint8_t ep) {
    readlen += 1;
    uint16_t len = usbd_ep_read_packet(dev, ep, &readbuf, sizeof(readbuf));
    if (len > 0) readlen += len;
}*/

/* ===== Exported API ===== */

void hidgp_set_usb_config(usbd_device *dev, uint16_t wValue)
{
    (void)wValue;

    // Make sure the gamepad entries are zeroed out
    memset(&_gp_reports, 0, sizeof(_gp_reports));

    // Set up the report IDs
    /*
    _gp_reports[0].report_id = 1;
    _gp_reports[1].report_id = 1;
    */

    // Do the USB setup
    usbd_ep_setup(dev, USBHID_EP_BASEADDR + 0, USB_ENDPOINT_ATTR_INTERRUPT, USBHID_GP_MAX_SIZE, NULL);
    usbd_ep_setup(dev, USBHID_EP_BASEADDR + 1, USB_ENDPOINT_ATTR_INTERRUPT, USBHID_GP_MAX_SIZE, NULL);

    // Register the control callback
    usbd_register_control_callback(
        dev,
        USB_REQ_TYPE_STANDARD | USB_REQ_TYPE_INTERFACE,
        USB_REQ_TYPE_TYPE | USB_REQ_TYPE_RECIPIENT,
        _hidgp_control_request);
}

bool hidgp_gamepad_state_udpated(uint32_t gpnum) {
    // Consider wrong GP number a fault
    if (gpnum >= WIICTRL_COUNT) return true;

    // Flag the state for beeing updated
    _hidgp_state_updated[gpnum] = true;    

    // Return the current fault flag state
    return _hidgp_fault_flags[gpnum];
}

bool hidgp_gamepad_transmit_next(usbd_device *dev) {
    static uint32_t ix = 0;

    // Depletion to avoid infinite loops
    uint32_t maxchecks = WIICTRL_COUNT;
    while (maxchecks--) {
        // Grab current index, and move on to next        
        uint32_t cix = ix++;
        if (ix >= WIICTRL_COUNT) ix = 0;

        // Check if state is updated
        if (_hidgp_state_updated[cix]) {
            // Clear the state
            _hidgp_state_updated[cix] = false;

            // Write the packet
            uint16_t len = usbext_ep_write_packet(dev, USBHID_EP_BASEADDR + cix, 
                                                  &_gp_reports[cix], USBHID_GP_REPORT_SIZE);

            // Set the fault flag if length is 0.
            _hidgp_fault_flags[cix] = (len == 0);
            return true;
        }
    }

    // Nothing to send, return false;
    return false;
}

void hidgp_dump_states(void) {
    struct wiictrl_gamepad* gp;
    
    for (int i = 0; i < 2; i++) {
        gp = &_gp_reports[i];
        printf("J[%d]: L(%d, %d), R(%d, %d), T(%d, %d), B(%04X)\r\n", (i+1),
                gp->lx, gp->ly, gp->rx, gp->ry, gp->lt, gp->rt, gp->buttons);
    }
}