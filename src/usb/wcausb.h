/**
 * usb/usb.h
 * 
 * Top level API for the USB implementation
 */

#ifndef _USB_USB_H
#define _USB_USB_H

#include <usb/usbext.h>

/**
 * Initializes the USB functionality of the
 */
void wcausb_init(bool enable_irq);

/**
 * Call this often
 */
void wcausb_poll(void);


#endif